Meteor.publish('system.notifications', function(amount, type) {
    amount = amount || 8;
    type = type || 'all';

    if (type != 'all') {
        return Notifications.find({type: type}, {sort: {date: -1}, limit: amount});
    } else {
        return Notifications.find({}, {sort: {date: -1}, limit: amount});
    }
});

Meteor.publish('system.latestNotifications', function() {
    return Notifications.find({
        isRead: false,
        date: {
            $gte: moment().subtract(1, 'days').toDate()
        }
    }, {sort: {date: -1}, limit: 4});
});

Meteor.publish('system.notificationCount', function() {
    var self = this;
    var count = 0;
    var initializing = true;

    // observeChanges only returns after the initial `added` callbacks
    // have run. Until then, we don't want to send a lot of
    // `self.changed()` messages - hence tracking the
    // `initializing` state.
    var handle = Notifications.find({
        isRead: false,
        date: {
            $gte: moment().subtract(1, 'days').toDate()
        }
    }).observeChanges({
        added: function(id) {
            count++;
            if (!initializing)
                self.changed('new-notifications-count', 1, {count: count});
        },
        removed: function(id) {
            count--;
            self.changed('new-notifications-count', 1, {count: count});
        }
        // don't care about changed
    });

    // Instead, we'll send one `self.added()` message right after
    // observeChanges has returned, and mark the subscription as
    // ready.
    initializing = false;
    self.added('new-notifications-count', 1, {count: count});
    self.ready();

    // Stop observing the cursor when client unsubs.
    // Stopping a subscription automatically takes
    // care of sending the client any removed messages.
    self.onStop(function() {
        handle.stop();
    });
});