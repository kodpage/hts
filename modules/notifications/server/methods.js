Meteor.methods({
    'system.notifications.read': function(id) {
        Notifications.update({_id: id}, {$set: {isRead: true}});
    },

    'system.notifications.new': function(type) {
        if (type && type != 'all') {
            return Notifications.find({
                isRead: false,
                type: type,
                date: {
                    $gte: moment().subtract(1, 'days').toDate()
                }}, {sort: {date: -1}, limit: 4}).count();
        } else {
            return Notifications.find({
                isRead: false,
                date: {
                    $gte: moment().subtract(1, 'days').toDate()
                }}, {sort: {date: -1}, limit: 4}).count();
        }
    }
});