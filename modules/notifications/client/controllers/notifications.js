NotificationsController = PanelController.extend({
    template: 'notificationsPage',

    waitOn: function() {
        return [
            Subs.subscribe('system.notifications'),
            AssetLoader.load('/assets/css/pages/mailbox.css')
        ];
    },

    action: function() {
        this.render();
    }

});