Template.notificationsDetail.helpers({
    typeBgColor: function() {
        var color = '';

        switch (this.type) {
            case 'system':
                color = 'bg-green-600';
                break;
            case 'warning':
                color = 'bg-orange-600';
                break;
            case 'error':
                color = 'bg-red-600';
                break;
            case 'info':
                color = 'bg-blue-600';
                break;
        }

        return color;
    },

    typeIcon: function() {
        var icon = '';

        switch (this.type) {
            case 'system':
                icon = 'wb-grid-9 bg-green-600';
                break;
            case 'warning':
                icon = 'wb-alert bg-orange-600';
                break;
            case 'error':
                icon = 'wb-close bg-red-600';
                break;
            case 'info':
                icon = 'wb-info bg-blue-600';
                break;
        }

        return icon;
    },

    typeColor: function() {
        var color = '';

        switch (this.type) {
            case 'system':
                color = 'green-600';
                break;
            case 'warning':
                color = 'orange-600';
                break;
            case 'error':
                color = 'red-600';
                break;
            case 'info':
                color = 'blue-600';
                break;
        }

        return color;
    },

    typeLabel: function() {
        var label = '';

        switch (this.type) {
            case 'system':
                label = 'Sisttem';
                break;
            case 'warning':
                label = 'Uyarı';
                break;
            case 'error':
                label = 'Hata';
                break;
            case 'info':
                label = 'Bilgi';
                break;
        }

        return label;
    },

    timeAgo: function() {
        return moment(this.date).fromNow();
    }

});

Template.notificationsDetail.onRendered(function() {
    Meteor.call('system.notifications.read', Template.currentData()._id)
});