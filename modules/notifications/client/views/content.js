Template.notificationsContent.helpers({
    notifications: function() {
        var amount = Session.get('system.notifications.limit') || 8,
            type = Session.get('system.notifications.type') || 'all';


        if (type != 'all') {
            return Notifications.find({type: type}, {sort: {date: -1}, limit: amount}).fetch();
        } else {
            return Notifications.find({}, {sort: {date: -1}, limit: amount}).fetch();
        }
    },

    moreResults: function() {
        var amount = Session.get('system.notifications.limit') || 8,
            type = Session.get('system.notifications.type') || 'all',
            count = 0;

        if (type != 'all') {
            count = Notifications.find({type: type}, {sort: {date: -1}, limit: amount}).count();
        } else {
            count = Notifications.find({}, {sort: {date: -1}, limit: amount}).count();
        }
        return !(count < Session.get('system.notifications.limit'));
    },

    abstract: function(content) {
        if (content.length >= 80)
            return content.substring(0, 80) + '...';
        else
            return content;
    },

    isSeen: function() {
        return this.isRead
    },

    typeIcon: function() {
        var icon = '';

        switch (this.type) {
            case 'system':
                icon = 'wb-grid-9 bg-green-600';
                break;
            case 'warning':
                icon = 'wb-alert bg-orange-600';
                break;
            case 'error':
                icon = 'wb-close bg-red-600';
                break;
            case 'info':
                icon = 'wb-info bg-blue-600';
                break;
        }

        return icon;
    },

    typeColor: function() {
        var color = '';

        switch (this.type) {
            case 'system':
                color = 'green-600';
                break;
            case 'warning':
                color = 'orange-600';
                break;
            case 'error':
                color = 'red-600';
                break;
            case 'info':
                color = 'blue-600';
                break;
        }

        return color;
    },

    typeLabel: function() {
        var label = '';

        switch (this.type) {
            case 'system':
                label = 'Sisttem';
                break;
            case 'warning':
                label = 'Uyarı';
                break;
            case 'error':
                label = 'Hata';
                break;
            case 'info':
                label = 'Bilgi';
                break;
        }

        return label;
    },

    timeAgo: function() {
        return moment(this.date).fromNow();
    }
});

var openPanel = function(context) {
    var options = {
        content: '',
        template: function() {
            return '<div class="slidePanel slidePanel-right">' +
                '<div class="slidePanel-scrollable"><div>' +
                '<div class="slidePanel-content"></div>' +
                '</div></div>' +
                '<div class="slidePanel-handler"></div>' +
                '</div>';
        },
        afterLoad: function() {
            this.$panel.find('.slidePanel-scrollable').asScrollable({
                namespace: 'scrollable',
                contentSelector: '>',
                containerSelector: '>'
            });
        },
        closeSelector: '.slidePanel-close',
        mouseDragHandler: '.slidePanel-handler'
    };

    $.slidePanel.show({
        static: true,
        settings: {
            cache: false
        }
    }, options);

    Blaze.renderWithData(Template.notificationsDetail, context, $('.slidePanel-content')[0]);
};

Template.notificationsContent.events({
    'click tr[data-toggle="slidePanel"]': function(e, t) {
        openPanel(this);
    }
});

Template.notificationsContent.onRendered(function() {
    var $content = this.$('.page-content-table'),
        rowHeight = this.$('tr[data-toggle="slidePanel"]:first').height(),
        limit = 8,
        type = 'all',
        id = Router.current().params._id;

    $('html').addClass('app-mailbox');

    $content.height(rowHeight * 8);

    $content.asScrollable({
        namespace: "scrollable",
        contentSelector: "> [data-role='content']",
        containerSelector: "> [data-role='container']",
        skin: "scrollable-shadow"
    });

    Session.set('system.notifications.limit', limit);
    Session.set('system.notifications.type', type);

    $content.on('asScrollable::scrollend', function(e, api, direction) {
        limit = limit + 8;

        Session.set('system.notifications.limit', limit);
    });

    if (id) {
        var context = Notifications.findOne({_id: id});
        openPanel(context);
    }
});

Template.notificationsContent.onDestroyed(function() {
    $('html').removeClass('app-mailbox');
});

Tracker.autorun(function() {
    Meteor.subscribe('system.notifications', Session.get('system.notifications.limit'), Session.get('system.notifications.type'));
});