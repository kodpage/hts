Template.notificationsAside.helpers({
    isActive: function(type) {
        return  Session.get('system.notifications.type') == type;
    }
});
Template.notificationsAside.events({
    'click .list-group-item': function(e, t) {
        var $this = $(e.target);

        if (!$this.is('.list-group-item')) {
            $this = $this.closest('.list-group-item');
        }

        var type = $this.data('type');

        Session.set('system.notifications.type', type);
        Session.set('system.notifications.limit', 8);
    }
});