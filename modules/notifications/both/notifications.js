Schemas.notifications = {};

Schemas.notifications.main = new Schema({
    title: {
        type: String,
        label: 'Başlık'
    },
    content: {
        type: String,
        label: 'İçerik'
    },
    type: {
        type: String,
        label: 'Tip',
        allowedValues: ['warning', 'info', 'error', 'system']
    },
    date: {
        type: Date,
        label: 'Tarih',
        defaultValue: moment().toDate()
    },
    isRead: {
        type: Boolean,
        label: 'Okundu',
        defaultValue: false
    }
});

Notifications = Model.extend('notifications', {
    schema: Schemas.notifications.main,
    helpers: {
    }
});