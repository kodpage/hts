var generateBulls = function() {
    var strPool = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

    Bulls.remove({});

    for (var i = 1; i < 50; i++) {
        var data = {},
            year = Chance.pick([2014, 2015]);

        data.firm = Chance.sentence({words: 2});

        do {
            data.acquisitionDate = Chance.date({year: 2015});
        } while (moment(data.acquisitionDate).diff(moment(), 'days') > 0);

        data.birthDate = data.acquisitionDate;
        data.midwife = 'Mahmut AKTAŞ';
        data.motherIdentity = Chance.string({length: 11, pool: strPool});

        do {
            data.identity = Chance.string({length: 11, pool: strPool});
        } while (Bulls.find({identity: data.identity}).count() > 0);

        data.name = Chance.name();

        Bulls.insert(data);
    }
};

var generateSperms = function() {
    var strPool = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

    Sperms.remove({});

    for (var i = 1; i < 50; i++) {
        var data = {};

        data.firm = Chance.sentence({words: 2});

        do {
            data.acquisitionDate = Chance.date({year: 2015});
        } while (moment(data.acquisitionDate).diff(moment(), 'days') > 0);

        do {
            data.identity = Chance.string({length: 11, pool: strPool});
        } while (Sperms.find({identity: data.identity}).count() > 0);

        data.name = Chance.name();
        data.quantity = Chance.pick([5, 7, 9, 15, 22]);

        Sperms.insert(data);
    }
};

var generateCows = function() {
    var strPool = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

    // Groups
    Groups.remove({});
    Groups.insert({name: 'Sağmal Grubu'});
    Groups.insert({name: 'Genç Hayvan Grubu'});
    Groups.insert({name: 'Diğer Hayvan Grubu'});

    Cows.remove({});

    // Cows
    for (var i = 1; i < 501; i++) {
        var data = {};

        // Get reg no
        data.registerNo = i;

        // Get identity
        do {
            data.identity = Chance.string({length: 11, pool: strPool});
        } while (Cows.find({identity: data.identity}).count() > 0);

        // Get rfid
        do {
            data.rfid = Chance.string({length: 11, pool: strPool});
        } while (Cows.find({identity: data.rfid}).count() > 0);

        // Extra
        do {
            var date = Chance.date({year: Chance.pick([2013, 2014, 2015])});
        } while (moment(date).diff(moment(), 'days') > 0);
        data.motherIdentity = Chance.string({length: 11, pool: strPool});
        data.fatherIdentity = Chance.string({length: 11, pool: strPool});
        data.fatherName = 'AGUSTOR';
        data.status = ['Boş'];
        data.joinReason = Chance.pick(['Doğum', 'Satın Alma']);
        data.joinDate = date;
        data.birthPlace = 'Sütkar';
        data.birthDate = date;
        data.birthType = Chance.pick(['Normal', 'Güç', 'Kendiliğinden']);
        data.birthWeight = Chance.integer({min: 18, max: 36});
        data.midwife = 'Mahmut AKTAŞ';
        data.note = Chance.paragraph({sentences: 1});

        // Find Group
        (function() {
            var groupId = '',
                a = moment(),
                b = moment(data.birthDate),
                age = a.diff(b, 'days');
            if (age <= 480) {
                groupId = Groups.findOne({name: 'Genç Hayvan Grubu'})._id;
            } else if (age > 480 && age < 730) {
                groupId = Groups.findOne({name: 'Sağmal Grubu'})._id;
            } else {
                groupId = Groups.findOne({name: 'Diğer Hayvan Grubu'})._id;
            }
            data.groupId = groupId;
        })();

        // Vaccinations
        (function() {
            data.vaccinations = [];
            for (var j = 1; j < Chance.integer({min: 5, max: 12}); j++) {
                var vaccinations = {};

                vaccinations.registerNo = j;
                vaccinations.date = Chance.date({year: 2015});
                vaccinations.name = Chance.pick([Chance.word(), Chance.word(),Chance.word(),Chance.word(),Chance.word(),Chance.word(),Chance.word(),Chance.word(),Chance.word()]);
                vaccinations.type = Chance.pick([Chance.word(), Chance.word(),Chance.word(),Chance.word(),Chance.word(),Chance.word(),Chance.word(),Chance.word(),Chance.word()]);
                vaccinations.appliedBy = Chance.name();
                vaccinations.not = Chance.sentence();

                data.vaccinations.push(vaccinations);
            }
        })();

        // Illness
        (function() {
            data.illnesses = [];
            for (var j = 1; j < Chance.integer({min: 2, max: 6}); j++) {
                var illness = {};

                illness.registerNo = j;
                illness.date = Chance.date({year: 2015});
                illness.illness = Chance.pick([Chance.word(), Chance.word(),Chance.word(),Chance.word(),Chance.word(),Chance.word(),Chance.word(),Chance.word(),Chance.word()]);
                illness.application = Chance.sentence();
                illness.result = Chance.sentence();
                illness.note = Chance.sentence();

                data.illnesses.push(illness);
            }
        })();

        // Insert cow
        Cows.insert(data);
    }
};

var generateChildren = function() {
    var appliedChildren = [];

    var getMaxChildBirthDate = function(mother) {
        var children = Cows.find({motherIdentity: mother.identity}).fetch();

        if (children && children.length > 0) {
            children = _.sortBy(children, 'birthDate');
            var child = children[children.length - 1];

            return moment(child.birthDate).add(360, 'days').toDate();
        } else {
            return moment(mother.birthDate).add(480, 'days').toDate();
        }
    };

    var generateInsemination = function(mother, child) {
        var insemination = {};

        insemination.registerNo = (mother.insemination) ? mother.insemination[mother.insemination.length - 1].registerNo : 1;
        insemination.date = moment(child.birthDate).subtract(360, 'days').toDate();
        insemination.appliedBy = Chance.name();
        insemination.type = Chance.pick(['sperm', 'bull']);

        var identities = [];
        if (insemination.type == 'bull') {
            identities = Bulls.find().fetch().map(function(doc) {
                return doc.identity;
            });
            insemination.identity = Chance.pick(identities);
        } else {
            identities = Sperms.find({quantity: {$gt: 0}}).fetch().map(function(doc) {
                return doc.identity;
            });
            if (identities.length > 0)
                insemination.identity = Chance.pick(identities);
        }

        Cows.update({_id: mother._id}, {$addToSet: {insemination: insemination}});
    };

    Cows.find({}, {sort: {birthDate: 1}}).forEach(function(mother) {
        // Find a suitable child for this mother
        do {
            var child = Cows.find({
                identity: {$nin: appliedChildren},
                birthDate: {
                    $gte: getMaxChildBirthDate(mother)
                }
            }, {sort: {birthDate: 1}}).fetch()[0];

            if (_.isObject(child)) {
                // Insemination
                generateInsemination(mother, child);

                Cows.update({_id: child._id}, {$set: {motherIdentity: mother.identity}});
                Cows.update({_id: mother._id}, {$addToSet: {childrenIdentities: child.identity}});

                // Set child as applied child
                appliedChildren.push(child.identity);
            }
        } while (_.isObject(child));
    });
};

var generateMilkData = function() {
    var cowCount = Cows.find({birthDate: {$lte: moment().subtract(480, 'days').toDate()}}).count(),
        ci = 1;

    MilkData.remove({});
    MilkDataSums.remove({});

    Cows.find({
        birthDate: {
            $lte: moment().subtract(480, 'days').toDate()
        }
    }, {sort: {birthDate: 1}}).forEach(function(cow) {
        process.stdout.write("\u001b[2J\u001b[0;0H");
        console.log('Generating milking data | ' + ci + '/' + cowCount + ' : ', (ci / cowCount * 100).toFixed(2));
        ci++;

        var ce = 10;
        var efficiencyEquation = function(day) {
            if (day >= 10 && day <= 60) {
                ce += 0.51;

                return ce;
            } else {
                ce -= 0.12;
            }

            return ce;
        };

        var generateMilk = function(startDate) {
            var endDate = moment(startDate).add(Chance.integer({min: 295, max: 305}), 'days').toDate(),
                currentDate = startDate;

            ce = 10;

            do {
                var amount = efficiencyEquation(moment(currentDate).diff(moment(startDate), 'days'));

                MilkData.insert({
                    rfid: cow.rfid,
                    date: currentDate,
                    amount: amount
                });

                currentDate = moment(currentDate).add(1, 'days').toDate();
            } while (moment(endDate).diff(moment(currentDate), 'days') >= 0 && moment().diff(moment(currentDate), 'days') >= 0);
        };

        _.each(cow.children(), function(child) {
            generateMilk(child.birthDate);
        });
    });
};

var generateStepCounts = function() {
    var cowCount = 150,
        ci = 1;

    StepCounts.remove({});

    Cows.find({}, {sort: {birthDate: 1}, limit: cowCount}).forEach(function(cow) {
        process.stdout.write("\u001b[2J\u001b[0;0H");
        console.log('Generating step counts | ' + ci + '/' + cowCount + ' : ', (ci / cowCount * 100).toFixed(2));
        ci++;

        var endDate = moment(),
            currentDate = moment().subtract(3, 'days'),
            lastChildBirthDate = (cow.hasChild()) ? cow.children()[0].birthDate : null;

        var isInHead = function(currentDate) {
            var elapsed = 0;

            if (lastChildBirthDate) {
                elapsed = currentDate.diff(moment(lastChildBirthDate), 'days');
            } else {
                elapsed = currentDate.diff(currentDate.subtract(480, 'days'), 'days');
            }

            var reminder = elapsed % 21;

            return (reminder < 2 || reminder > 18);
        };

        do {
            var count = 0;

            if (isInHead(currentDate)) {
                count = Chance.integer({min: 90, max: 105});

                var tempDate = moment(currentDate.toDate());

                var md = MilkData.findOne({
                    rfid: cow.rfid,
                    date: {
                        $lte: tempDate.hour(23).minute(59).toDate(),
                        $gte: tempDate.hour(0).minute(0).toDate()
                    }
                });

                if (md) {
                    var newAmount = md.amount - (((count - 80) / 80) * md.amount);

                    MilkData.update({
                        _id: md._id
                    }, {
                        $set: {
                            amount: newAmount
                        }
                    })
                }
            } else {
                count = Chance.integer({min: 75, max: 85});
            }

            StepCounts.insert({
                identity: cow.identity,
                rfid: cow.rfid,
                date: currentDate.toDate(),
                count: count
            });

            currentDate = currentDate.add(1, 'hours');
        } while (endDate.diff(currentDate) > 0);
    });
};

//setInterval(function() {
//    Notifications.remove({});
//
//    console.log('asdaf');
//
//    for (var i = 0; i < 200; i++) {
//        var date = null;
//        do {
//            date = Chance.date({year: 2015, month: 11});
//        } while (moment(date).diff(moment()) > 0);
//
//        Notifications.insert({
//            title: Chance.sentence({words: Chance.pick([2, 3, 4, 5])}),
//            content: Chance.paragraph(),
//            type: Chance.pick(['warning', 'info', 'error', 'system']),
//            date: date,
//            isRead: Chance.pick([true, false])
//        });
//    }
//}, 1000*60*60);

//generateBulls();
//generateSperms();
//generateCows();
//generateChildren();
//generateMilkData();
//generateStepCounts();


//console.log('Fake data generation is completed.');

//Router.route('/fixture', function() {
//
//    generateBulls();
//    generateSperms();
//    generateCows();
//    generateChildren();
//    generateMilkData();
//    generateStepCounts();
//
//    console.log('Fake data generation is completed.');
//
//    this.response.end('Fake data generation is completed.\n');
//}, {where: 'server'});