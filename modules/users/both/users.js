Schemas.users = {};

Schemas.users.profile = new Schema({
    fullName: {
        type: String
    },
    identity: {
        type: String,
        optional: true
    },
    phone: {
        type: String,
        optional: true
    }
});

Schemas.users.main = new Schema({
    username: {
        type: String,
        regEx: /^[a-z0-9A-Z_]{3,15}$/
    },
    emails: {
        type: [Object],
        // this must be optional if you also use other login services like facebook,
        // but if you use only accounts-password, then it can be required
        optional: true
    },
    "emails.$.address": {
        type: String,
        regEx: Schema.RegEx.Email
    },
    "emails.$.verified": {
        type: Boolean
    },
    createdAt: {
        type: Date
    },
    profile: {
        type: Schemas.users.profile,
        optional: true
    },
    services: {
        type: Object,
        optional: true,
        blackbox: true
    },
    permissions: {
        type: [String],
        optional: true
    },
    settings: {
        type: Object,
        optional: true,
        blackbox: true
    },
    data: {
        type: Object,
        optional: true,
        blackbox: true
    }
});

// Attach the schema to the users collection
Meteor.users.attachSchema(Schemas.users.main);

Tables.users = new Tabular.Table({
    name: 'system.users',
    collection: Meteor.users,
    columns: [
        {data: 'profile.fullName', title: 'Ad / Soyad'},
        {data: 'profile.phone', title: 'Telefon'},
        {data: 'username', title: 'Kullanıcı Adı'},
        {data: 'emails.0.address', title: 'E-Posta Adresi'},
        {data: 'createdAt', title: 'Oluşturuldu', render: function(data) {
            return moment(new Date(data)).format('DD-MM-YYYY H:mm:ss');
        }}
    ]
    //rowClick: function(data) {
    //    Router.go('management.users.update', {_id: data._id, section: 'profile'});
    //}
});

Tables.userLogs = new Tabular.Table({
    name: 'system.users.logs',
    collection: Logs._collection,
    columns: [
        {data: 'date', title: 'Tarih', render: function(data) {
            return moment(data).format('DD-MM-YYYY H:mm:ss');
        }},
        {data: 'type', title: 'Seviye', render: function(type) {
            switch (type) {
                case 'warning':
                    return '<span class="label label-warning">Uyarı</span>';
                    break;

                case 'info':
                    return '<span class="label label-info">Bilgi</span>';
                    break;

                case 'error':
                    return '<span class="label label-error">Hata</span>';
                    break;
            }
        }},
        {data: 'msg', title: 'Kayıt'}
    ],
    rowClick: function(data) {
        return false;
    },
    autoWidth: false,
    checkboxCell: false
});