Meteor.publish('system.users.user', function(userId) {
    return Meteor.users.find({_id: userId}, {
        fields: {
            'username': 1,
            'emails': 1,
            'profile': 1,
            'createdAt': 1,
            'settings': 1,
            'permissions': 1,
            'data': 1
        }
    });
});
Meteor.publish('system.users.users', function(userIds) {
    if (_.isArray(userIds)) {
        return Meteor.users.find({_id: {$in: userIds}});
    } else {
        return Meteor.users.find({});
    }
});