// Create admin user
var admin = Meteor.users.findOne({username: 'admin'});

if (!admin) {
    var userId = Accounts.createUser({
        username: 'admin',
        email: 'admin@sutkar.com.tr',
        password: 'admin',
        createdAt: Date.now(),
        profile: {
            fullName: 'System Admin'
        }
    });

    Permission.addUserPermission(userId, 'admin');
}