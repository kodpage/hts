Meteor.users.allow({
    remove: function(userId, doc) {
        return Meteor.user().hasPermission('system.users.delete');
    }
});