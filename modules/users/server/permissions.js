Permission.add({
    name: 'system.users',
    label: 'Kullanıcı listesi erişimi',
    group: 'Kullanıcı Sistemi'
});
Permission.add({
    name: 'system.users.create',
    label: 'Kullanıcı oluştur',
    group: 'Kullanıcı Sistemi'
});
Permission.add({
    name: 'system.users.update',
    label: 'Kullanıcı düzenle',
    group: 'Kullanıcı Sistemi'
});
Permission.add({
    name: 'system.users.delete',
    label: 'Kullanıcı sil',
    group: 'Kullanıcı Sistemi'
});