var checkPermission = function(permission) {
    var result = Meteor.user().hasPermission(permission);

    if (! result) {
        throw new Meteor.Error('Bu işlemi gerçekleştirmek için gerekli izine sahip değilsiniz!');
    }
};

var isAdmin = function(id) {
    var user = Meteor.users.findOne({_id: id});

    if (!_.isObject(user)) {
        throw new Meteor.Error('User not found');
    }

    return _.contains(user.permissions, 'admin');
};

Meteor.methods({
    'system.users.create': function(data) {
        // Check permissions
        checkPermission('system.users.create');

        // Create system user
        var userId = Accounts.createUser({
            username: data.username,
            email: data.email,
            password: data.password,
            createdAt: Date.now(),
            profile: {
                fullName: data.fullName,
                phone: data.phone,
                identity: data.identity
            }
        });
        Permission.addUserPermission(userId, data.permissions);
    },

    'system.users.update': function(data, id) {
        data = data.$set;

        Meteor.users.update({_id: id}, {$set: {
            username: data.username,
            'emails.0.address': data.email,
            profile: {
                fullName: data.fullName,
                phone: data.phone,
                identity: data.identity
            }
        }});

        if (data.password) {
            Accounts.setPassword(id, data.password);
        }

        if (data.permissions && _.isArray(data.permissions)) {
            if (isAdmin(id)) {
                throw new Meteor.Error('Admin izinleri değiştirilemez.');
            } else {
                Meteor.users.update({_id: id}, {$set: {
                    permissions: data.permissions
                }});
            }
        }
    }
});