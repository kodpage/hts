UsersProfileController = PanelController.extend({
    template: 'usersProfilePage',

    waitOn: function() {
        return Subs.subscribe('system.users.user', this.params._id);
    },

    data: function () {
        return Meteor.users.findOne({_id: this.params._id});
    },

    action: function() {
        this.render();
    }
});