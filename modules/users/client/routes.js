Router.route('/system/users', {
    name: 'system.users',
    title: 'Kullanıcılar',
    controller: UsersController
});
Router.route('/system/users/:_id/profile', {
    name: 'system.users.profile',
    title: 'Kullanıcı Profili',
    controller: UsersProfileController
});
Router.route('/system/users/create', {
    name: 'system.users.create',
    title: 'Kullanıcı Oluştur',
    parent: 'system.users',
    controller: UsersCreateController
});
Router.route('/system/users/update/:_id/:section?/:stage?/:subId?', {
    name: 'system.users.update',
    title: 'Kullanıcıyı Düzenle',
    parent: 'system.users',
    controller: UsersUpdateController
});