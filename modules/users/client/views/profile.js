AutoForm.hooks({
    usersProfileForm: {
        docToForm: function(doc) {
            doc.fullName = doc.profile.fullName;
            doc.identity = doc.profile.identity;
            doc.email = doc.emails[0].address;
            doc.phone = doc.profile.phone;

            return doc;
        },
        before: {
            'method-update': function(doc) {
                Logs.info(doc.username + ' kullanıcı adına sahip kullanıcının bilgileri güncellendi.');

                return doc;
            }
        },

        onSuccess: function(formType, result) {
            Notify.success('Bilgileriniz başarıyla güncellendi.');
        }
    }
});

Template.usersProfilePage.helpers({
    schema: function() {
        var schema = new Schema({
            username: {
                type: String,
                label: 'Kullanıcı Adı',
                regEx: /^[a-z0-9A-Z_]{3,15}$/,
                min: 3
            },
            email: {
                type: String,
                label: 'E-Posta Adresi',
                regEx: Schema.RegEx.Email
            },
            password: {
                label: 'Şifre',
                type: String,
                min: 6,
                optional: true
            },
            confirmation: {
                label: 'Şifre Tekrar',
                type: String,
                min: 6,
                optional: true,
                custom: function() {
                    if (this.value !== this.field('password').value) {
                        return 'noMatch';
                    }
                }
            },
            fullName: {
                label: 'Ad / Soyad',
                type: String
            },
            identity: {
                label: 'TC Kimlik No',
                type: String,
                optional: true
            },
            phone: {
                label: 'Telefon',
                type: String,
                optional: true
            }
        });

        schema.messages({
            noMatch: 'Şifreler eşleşmelidir!'
        });

        return schema;
    },

    table: function() {
        return Tables.userLogs
    },

    selector: function() {
        return {userId: Router.current().params._id}
    }
});

Template.usersProfilePage.onRendered(function() {
    $('.btn-cancel').hide();
});