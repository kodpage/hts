Template.usersUpdatePage.helpers({
    isActive: function(section) {
        var currentSection = Router.current().params.section || 'settings';

        return section == currentSection;
    },
    template: function() {
        var section = Router.current().params.section || 'settings';

        section = section.charAt(0).toUpperCase() + section.slice(1);

        return 'usersUpdate' + section;
    }
});