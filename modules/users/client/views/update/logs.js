Template.usersUpdateLogs.helpers({
    table: function() {
        return Tables.userLogs
    },

    selector: function() {
        return {userId: Router.current().params._id}
    }
});