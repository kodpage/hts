var isAdmin = false;

AutoForm.hooks({
    updateUserForm: {
        docToForm: function(doc) {
            doc.fullName = doc.profile.fullName;
            doc.identity = doc.profile.identity;
            doc.email = doc.emails[0].address;
            doc.phone = doc.profile.phone;

            return doc;
        },
        before: {
            'method-update': function(doc) {
                // Permissions
                if (!isAdmin) {
                    doc.$set.permissions = [];
                    $('.permission-input').each(function() {
                        var $this = $(this);

                        if ($this.is(':checked')) {
                            doc.$set.permissions.push($this.val());
                        }
                    });
                }

                Logs.info(doc.username + ' kullanıcı adına sahip kullanıcının bilgileri güncellendi.');

                return doc;
            }
        },

        onSuccess: function(formType, result) {
            Notify.success('Kullanıcı ayarları başarıyla güncellendi.');
            Router.go('system.users');
        }
    }
});

Template.usersUpdateSettings.helpers({
    schema: function() {
        var schema = new Schema({
            username: {
                type: String,
                label: 'Kullanıcı Adı',
                regEx: /^[a-z0-9A-Z_]{3,15}$/,
                min: 3
            },
            email: {
                type: String,
                label: 'E-Posta Adresi',
                regEx: Schema.RegEx.Email
            },
            password: {
                label: 'Şifre',
                type: String,
                min: 6,
                optional: true
            },
            confirmation: {
                label: 'Şifre Tekrar',
                type: String,
                min: 6,
                optional: true,
                custom: function() {
                    if (this.value !== this.field('password').value) {
                        return 'noMatch';
                    }
                }
            },
            fullName: {
                label: 'Ad / Soyad',
                type: String
            },
            identity: {
                label: 'TC Kimlik No',
                type: String,
                optional: true
            },
            phone: {
                label: 'Telefon',
                type: String,
                optional: true
            },
            permissions: {
                type: [String],
                optional: true
            }
        });

        schema.messages({
            noMatch: 'Şifreler eşleşmelidir!'
        });

        return schema;
    },

    groups: function() {
        var items = _.keys(_.groupBy(Permission._collection.find({}).fetch(), 'group')),
            groups = [];

        _.each(items, function(item) {
            groups.push({name: item});
        });

        return groups;
    },

    permissions: function(group) {
        var data = _.groupBy(Permission._collection.find({}).fetch(), 'group');

        return data[group];
    }
});

Template.usersUpdateSettings.onRendered(function() {
    // Selected Permissions
    var permissions = this.data.permissions;
    if (permissions) {
        if (permissions[0] == 'admin') {
            isAdmin = true;

            var $input = $('.permission-input');
            $input.prop('checked', true);
        } else {
            _.each(permissions, function(permission) {
                var $input = $('input.permission-input[value="' + permission + '"]');
                $input.prop('checked', true);
            });
        }
    }

    this.$('.permission-box').matchHeight();
});