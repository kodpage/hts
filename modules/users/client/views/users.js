Template.usersPage.helpers({
    table: function() {
        return Tables.users
    },

    deleteAction: function() {
        return {
            msg: 'Kullanıcı(ları) silmek istediğinize emin misiniz?',
            callback: function(isConfirmed) {
                if (isConfirmed) {
                    var users = Session.get('system.users.selectedRows'),
                        hasError = false;

                    for (var i = 0; i < users.length; i++) {
                        if (Meteor.userId() == users[i]._id) {
                            hasError = true;
                            break;
                        } else {
                            Meteor.users.remove({_id: users[i]._id});
                        }
                    }

                    if (hasError) {
                        Notify.error('Kendi kullanıcı hesabınızı silemezsiniz!');
                    } else {
                        Notify.success('Kullanıcı(lar) başarıyla silindi.');
                    }
                }
            }
        };
    }
});

Template.usersPage.onRendered(function() {

});