AutoForm.hooks({
    createUserForm: {
        before: {
            method: function(doc) {
                // Permissions
                doc.permissions = [];
                $('.permission-input').each(function() {
                    var $this = $(this);

                    if ($this.is(':checked')) {
                        doc.permissions.push($this.val());
                    }
                });

                Logs.info(doc.username + ' kullanıcı adına sahip yeni bir kullanıcı oluşturuldu.');

                return doc;
            }
        },
        onSuccess: function(formType, result) {
            Notify.success('Kullanıcı başarıyla oluşturuldu.');
            Router.go('system.users');
        }
    }
});

Template.usersCreatePage.helpers({
    schema: function() {
        var schema = new Schema({
            username: {
                type: String,
                label: 'Kullanıcı Adı',
                regEx: /^[a-z0-9A-Z_]{3,15}$/,
                min: 3
            },
            email: {
                type: String,
                label: 'E-Posta Adresi',
                regEx: Schema.RegEx.Email
            },
            password: {
                label: 'Şifre',
                type: String,
                min: 6
            },
            confirmation: {
                label: 'Şifre Tekrar',
                type: String,
                min: 6,
                custom: function() {
                    if (this.value !== this.field('password').value) {
                        return 'noMatch';
                    }
                }
            },
            fullName: {
                label: 'Ad / Soyad',
                type: String
            },
            identity: {
                label: 'TC Kimlik No',
                type: String,
                optional: true
            },
            phone: {
                label: 'Telefon',
                type: String,
                optional: true
            },
            permissions: {
                type: [String],
                optional: true
            }
        });

        schema.messages({
            noMatch: 'Şifreler eşleşmelidir!'
        });

        return schema;
    },

    groups: function() {
        var items = _.keys(_.groupBy(Permission._collection.find({}).fetch(), 'group')),
            groups = [];

        _.each(items, function(item) {
            groups.push({name: item});
        });

        return groups;
    },

    permissions: function(group) {
        var data = _.groupBy(Permission._collection.find({}).fetch(), 'group');

        return data[group];
    }
});