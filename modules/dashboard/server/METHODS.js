Meteor.methods({
    'farming.dashboard.monthlyAverage': function() {
        this.unblock();

        var startDate = moment(moment().year() + '-01-01 00:00:00').toDate(),
            endDate = moment().toDate(),
            monthPassed = moment().month() + 1,
            total = 0;

        MilkDataSums.find({
            date: {
                $gte: startDate,
                $lte: endDate
            }
        }, {sort: {date: -1}}).forEach(function(d) {
            total += d.amount;
        });

        return (total / monthPassed).toFixed(2);
    }
});