Meteor.publish('farming.dashboard.stats', function() {
    var self = this;
    var initializing = true;

    var data = {
        milkedCows: 0,
        youngCows: 0,
        pregnantCows: 0,
        kurudaCount: 0,
        newBornCount: 0,
        cowsTotal: 0,
        bullsTotal: 0,
        totalAnimal: 0,
        totalDeadCount: 0,
        currentYear: moment().year()
    };

    var calculateCounts = function(id, subtract) {
        var cow = Cows.findOne({_id: id});

        if (subtract) {
            if (cow.hasChild()) {
                if (_.contains(cow.status, 'Gebe')) {
                    data.pregnantCows--;

                    if (!_.contains(cow.status, 'Kuruda')) {
                        data.milkedCows--;
                    } else {
                        data.kurudaCount--;
                    }
                } else {
                    if (!_.contains(cow.status, 'Kuruda')) {
                        data.milkedCows--;
                    } else {
                        data.kurudaCount--;
                    }
                }
            } else {
                if (_.contains(cow.status, 'Gebe')) {
                    data.pregnantCows--;
                } else {
                    data.youngCows--;
                }
            }

            if (moment(cow.birthDate).diff(moment(moment().year() + '-01-01'), 'days') > 0) {
                data.newBornCount--;
            }

            data.cowsTotal--;
        } else {
            if (cow.hasChild()) {
                if (_.contains(cow.status, 'Gebe')) {
                    data.pregnantCows++;

                    if (!_.contains(cow.status, 'Kuruda')) {
                        data.milkedCows++;
                    } else {
                        data.kurudaCount++;
                    }
                } else {
                    if (!_.contains(cow.status, 'Kuruda')) {
                        data.milkedCows++;
                    } else {
                        data.kurudaCount++;
                    }
                }
            } else {
                if (_.contains(cow.status, 'Gebe')) {
                    data.pregnantCows++;
                } else {
                    data.youngCows++;
                }
            }

            if (moment(cow.birthDate).diff(moment(moment().year() + '-01-01'), 'days') > 0) {
                data.newBornCount++;
            }

            data.cowsTotal++;
        }

        data.youngCowsPercentage = parseInt((data.youngCows / data.cowsTotal) * 100);
        data.milkedCowsPercentage = parseInt((data.milkedCows / data.cowsTotal) * 100);
        data.pregnantCowsPercentage = parseInt((data.pregnantCows / data.cowsTotal) * 100);

        data.bullsTotal = Bulls.find().count();
        data.totalAnimal = data.cowsTotal + data.bullsTotal;

        data.totalDeadCount = RemovedCows.find({
            date: {
                $gte: moment(moment().year() + '-01-01').toDate()
            }
        }).count();
    };

    var handle = Cows.find({}).observeChanges({
        added: function(id) {
            calculateCounts(id);

            if (!initializing)
                self.changed('dashboard-cows-stats', 1, {data: data});
        },
        removed: function(id) {
            calculateCounts(id, true);
            self.changed('dashboard-cows-stats', 1, {data: data});
        }
    });

    initializing = false;
    self.added('dashboard-cows-stats', 1, {data: data});
    self.ready();

    self.onStop(function() {
        handle.stop();
    });
});

Meteor.publish('farming.dashboard.milking', function() {
    var startDate = moment().subtract(moment().date(), 'days').toDate(),
        endDate = moment().toDate();

    return MilkDataSums.find({
        date: {
            $gte: startDate,
            $lte: endDate
        }
    }, {sort: {date: -1}});
});