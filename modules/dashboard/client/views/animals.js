Template.dashboardAnimals.onRendered(function() {
    var stats = StatsDashboard.findOne({_id: 1}).data;

    if (_.isObject(stats) && stats.milkedCows > 0) {
        new Chartist.Pie('#chartPie .ct-chart', {
            series: [stats.milkedCowsPercentage, stats.youngCowsPercentage, (100 - (stats.youngCowsPercentage + stats.milkedCowsPercentage))]
        }, {
            donut: true,
            donutWidth: 10,
            startAngle: 0,
            showLabel: false
        });
    }
});