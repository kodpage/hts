Template.dashboardMilking.helpers({
    daikyUnitAverage: function() {
        var startDate = moment().subtract(moment().date(), 'days').toDate(),
            endDate = moment().toDate(),
            total = 0,
            milked = 0;

        MilkDataSums.find({
            date: {
                $gte: startDate,
                $lte: endDate
            }
        }).forEach(function(d) {
            total += d.amount;
            milked += d.rfids.length;
        });

        return (total / milked).toFixed(2);
    },

    dailyAverage: function() {
        var startDate = moment().subtract(moment().date(), 'days').toDate(),
            endDate = moment().toDate(),
            total = 0;

        MilkDataSums.find({
            date: {
                $gte: startDate,
                $lte: endDate
            }
        }).forEach(function(d) {
            total += d.amount;
        });

        return (total / moment().date()).toFixed(2);
    },

    monthlyAverage: function() {
        return ReactiveMethod.call('farming.dashboard.monthlyAverage');
    }
});

Template.dashboardMilking.onRendered(function() {
    var $container = this.$('#dashboardMilking'),
        startDate = moment().subtract(moment().date(), 'days').toDate(),
        endDate = moment().toDate(),
        total = 0,
        day = 0;

    var data = MilkDataSums.find({
        date: {
            $gte: startDate,
            $lte: endDate
        }
    }, {sort: {date: -1}}).fetch().map(function(d) {
        total += d.amount;
        day++;

        return {
            date: moment(d.date).format('YYYY-MM-DD'),
            amount: d.amount,
            average: (total / day)
        };
    });

    Morris.Line({
        element: 'milkingGraph',
        data: data,
        xkey: 'date',
        ykeys: ['amount', 'average'],
        labels: ['Miktar', 'Ortalama'],
        pointSize: 3,
        gridTextColor: '#474e54',
        gridLineColor: '#eef0f2',
        goalLineColors: '#e3e6ea',
        gridTextFamily: $.configs.get('site', 'fontFamily'),
        gridTextWeight: '500',
        gridtextSize: 18,
        lineWidth: 3,
        lineColors: [$.colors('primary', 800), $.colors('light-green', 800)],
        dateFormat: function(x) {
            return moment(x).format('DD.MM.YYYY');
        },
        yLabelFormat: function(y) {
            return y.toFixed(2) + ' Lt';
        }
    });
});