StatsDashboard = new Meteor.Collection('dashboard-cows-stats');

Template.dashboardPage.helpers({
    stats: function() {
        return StatsDashboard.findOne({_id: 1}).data;
    }
});

Template.dashboardPage.events({

});

Template.dashboardPage.onRendered(function() {
});