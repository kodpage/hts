DashboardController = PanelController.extend({
    template: 'dashboardPage',

    waitOn: function() {
        return [
            Subs.subscribe('farming.dashboard.stats'),
            Subs.subscribe('farming.dashboard.milking')
        ];
    },

    action: function() {
        this.render();
    }

});