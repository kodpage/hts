Meteor.methods({
    'system.settings.update': function(data) {
        data = data.$set;

        for (var name in data) {
            if (data.hasOwnProperty(name)) {
                Settings.update({name: name}, {$set: {value: data[name]}});
            }
        }
    }
});