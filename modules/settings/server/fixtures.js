if (Settings.find().count() < 1) {
    Settings.insert({name: 'farming.normalHourlyStepCount', value: 80});
    Settings.insert({name: 'farming.abnormalHourlyStepCount', value: 95});
    Settings.insert({name: 'farming.prepareDriedDay', value: 70});
    Settings.insert({name: 'farming.makeDriedDay', value: 60});
    Settings.insert({name: 'farming.takeToDeliveryRoom', value: 21});
    Settings.insert({name: 'farming.pregnancyDuration', value: 282});
    Settings.insert({name: 'farming.minHeiferInseminationAge', value: 450});
    Settings.insert({name: 'farming.firstInseminationAfterDelivery', value: 45});
}