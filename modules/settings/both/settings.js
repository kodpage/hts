Settings = new Mongo.Collection('settings');

Settings.get = function(name) {
    check(name, String);

    var setting = Settings.findOne({name: name});

    if (!_.isObject(setting)) {
        throw new Meteor.Error('Böyle bir ayar mevcut değil : ' + name);
    }

    return setting.value;
};