
AutoForm.hooks({
    updateSettingsForm: {
        onSuccess: function(formType, result) {
            Notify.success('Ayarlar başarıyla güncellendi.');
            Logs.info('Ayarlar güncellendi.');
        }
    }
});

Template.settingsPage.helpers({
    doc: function() {
        var doc = {};

        Settings.find().forEach(function(setting) {
           doc[setting.name] = setting.value;
        });

        return doc;
    },

    schema: function() {
        return new Schema({
            'farming.normalHourlyStepCount': {
                type: Number,
                label: 'İneğin saatlik normal adım sayısı'
            },

            'farming.abnormalHourlyStepCount': {
                type: Number,
                label: 'İneğin saatlik anormal adım sayısı'
            },

            'farming.prepareDriedDay': {
                type: Number,
                label: 'Kuruya hazırla'
            },

            'farming.makeDriedDay': {
                type: Number,
                label: 'Kuraya çıkar'
            },

            'farming.takeToDeliveryRoom': {
                type: Number,
                label: 'Doğumhaneye al'
            },

            'farming.pregnancyDuration': {
                type: Number,
                label: 'Gebelik süresi'
            },

            'farming.minHeiferInseminationAge': {
                type: Number,
                label: 'Minimum düve tohumlama yaşı'
            },

            'farming.firstInseminationAfterDelivery': {
                type: Number,
                label: 'Doğum sonrası ilk tohum sınırı'
            }
        });
    }
});

Template.settingsPage.onRendered(function() {
    $('.btn-cancel').hide();
});