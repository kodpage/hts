// Cows
Permission.add({
    name: 'farming.livestock.cows',
    label: 'İnek listesi erişimi',
    group: 'İnek Yönetimi'
});
Permission.add({
    name: 'farming.livestock.cows.create',
    label: 'İnek oluştur',
    group: 'İnek Yönetimi'
});
Permission.add({
    name: 'farming.livestock.cows.update',
    label: 'İnek düzenle',
    group: 'İnek Yönetimi'
});
Permission.add({
    name: 'farming.livestock.cows.delete',
    label: 'İnek sil',
    group: 'İnek Yönetimi'
});
Permission.add({
    name: 'farming.livestock.removed-cows',
    label: 'Çıkartılan inekler listesi erişimi',
    group: 'İnek Yönetimi'
});

// Groups
Permission.add({
    name: 'farming.livestock.groups',
    label: 'Grup erişimi',
    group: 'Grup Yönetimi'
});
Permission.add({
    name: 'farming.livestock.groups.create',
    label: 'Grup oluştur',
    group: 'Grup Yönetimi'
});
Permission.add({
    name: 'farming.livestock.groups.update',
    label: 'Grup güncelle',
    group: 'Grup Yönetimi'
});
Permission.add({
    name: 'farming.livestock.groups.delete',
    label: 'Grup sil',
    group: 'Grup Yönetimi'
});

// Bulls
Permission.add({
    name: 'farming.livestock.bulls',
    label: 'Boğa listesi erişimi',
    group: 'Boğa Yönetimi'
});
Permission.add({
    name: 'farming.livestock.bulls.create',
    label: 'Boğa oluştur',
    group: 'Boğa Yönetimi'
});
Permission.add({
    name: 'farming.livestock.bulls.update',
    label: 'Boğa güncelle',
    group: 'Boğa Yönetimi'
});
Permission.add({
    name: 'farming.livestock.bulls.delete',
    label: 'Boğa sil',
    group: 'Boğa Yönetimi'
});

// Sperms
Permission.add({
    name: 'farming.livestock.sperms',
    label: 'Sperm listesi erişimi',
    group: 'Sperm Yönetimi'
});
Permission.add({
    name: 'farming.livestock.sperms.create',
    label: 'Sperm oluştur',
    group: 'Sperm Yönetimi'
});
Permission.add({
    name: 'farming.livestock.sperms.update',
    label: 'Sperm güncelle',
    group: 'Sperm Yönetimi'
});
Permission.add({
    name: 'farming.livestock.sperms.delete',
    label: 'Sperm sil',
    group: 'Sperm Yönetimi'
});

Permission.add({
    name: 'farming.livestock.manuel-data',
    label: 'Manuel veri yönetimi',
    group: 'Genel'
});