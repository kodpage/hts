Meteor.methods({
    'farming.livestock.cows.getRegisterNo': function() {
        var cow = Cows.find({}, {limit: 1, sort: {registerNo: -1}}).fetch()[0];

        if (cow)
            return cow.registerNo + 1;
        else
            return 1;
    },

    'farming.livestock.cows.getGroupCowCount': function() {
        var groups = Groups.find({}).fetch(),
            data = {};

        _.each(groups, function(group) {
            data[group._id] = Cows.find({groupId: group._id}).count();
        });

        return data;
    },

    'farming.livestock.cows.report': function(id) {
        var data = {},
            cow = Cows.findOne({_id: id});

        if (cow) {
            data.children = cow.children() || [];
            data.lactionCount = cow.lactationCount();
            data.heiferInseminationDate = (_.isArray(cow.insemination) && cow.insemination.length > 0) ? cow.insemination[0].date : '';

            if (data.children.length > 0) {
                data.firstDeliveryDate = data.children[0].birthDate;
                data.firstDeliveryAge = moment(data.children[0].birthDate).diff(moment(cow.birthDate), 'days');
                data.lastDeliveryDate = data.children[data.children.length - 1].birthDate
            }
        }

        return data;
    },

    'farming.livestock.cows.exportMilkingData': function(rfid, startDate, endDate) {
        var excel = new Excel('xlsx'); // Create an excel object  for the file you want (xlsx or xls)
        var workbook = excel.createWorkbook(); // Create a workbook (equivalent of an excel file)
        var worksheet = excel.createWorksheet(); // Create a worksheet to be added to the workbook

        worksheet.writeToCell(0, 0, 'Tarih');
        worksheet.writeToCell(0, 1, 'Miktar (Lt)');
        worksheet.writeToCell(0, 2, 'Ortalama (Lt)');
        worksheet.writeToCell(0, 3, 'Toplam (Lt)');

        var days = 0,
            total = 0,
            row = 1;

        MilkData.find({
            rfid: rfid,
            date: {
                $gte: startDate,
                $lte: endDate
            }
        }, {sort: {date: 1}}).forEach(function(d) {
            days++;
            total += d.amount;

            worksheet.writeToCell(row, 0, moment(d.date).format('DD.MM.YYYY'));
            worksheet.writeToCell(row, 1, d.amount.toFixed(2));
            worksheet.writeToCell(row, 2, (total / days).toFixed(2));
            worksheet.writeToCell(row, 3, total.toFixed(2));

            row++;
        });

        workbook.addSheet('Süt Verimi', worksheet);

        var workBookOut = workbook.write({bookType: 'xlsx', bookSST: false, type: 'binary'});

        return new Buffer(workBookOut, 'binary').toString('base64');
    },

    'farming.livestock.cows.update.updateVaccination': function(id, regNo, fields) {
        Cows.update({_id: id, 'vaccinations.registerNo': regNo}, {
            $set: {
                'vaccinations.$.name': fields.name,
                'vaccinations.$.type': fields.type,
                'vaccinations.$.date': fields.date,
                'vaccinations.$.appliedBy': fields.appliedBy,
                'vaccinations.$.not': fields.not
            }
        });
    },

    'farming.livestock.cows.update.deleteVaccination': function(id, regNo) {
        Cows.update({_id: id}, {
            $pull: {
                vaccinations: {registerNo: regNo}
            }
        });
    },

    'farming.livestock.cows.update.decreaseSpermQuantity': function(fields) {
        Sperms.update({identity: fields.identity}, {$inc: {quantity: -1}});
    },

    'farming.livestock.cows.update.updateInsemination': function(id, regNo, fields) {
        Cows.update({_id: id, 'insemination.registerNo': regNo}, {
            $set: {
                'insemination.$.identity': fields.identity,
                'insemination.$.type': fields.type,
                'insemination.$.date': fields.date,
                'insemination.$.appliedBy': fields.appliedBy
            }
        });
    },

    'farming.livestock.cows.update.deleteInsemination': function(id, regNo) {
        Cows.update({_id: id}, {
            $pull: {
                insemination: {registerNo: regNo}
            }
        });
    },

    'farming.livestock.cows.update.updateIllness': function(id, regNo, fields) {
        Cows.update({_id: id, 'illnesses.registerNo': regNo}, {
            $set: {
                'illnesses.$.result': fields.result,
                'illnesses.$.illness': fields.illness,
                'illnesses.$.date': fields.date,
                'illnesses.$.application': fields.application,
                'illnesses.$.note': fields.note
            }
        });
    },

    'farming.livestock.cows.update.deleteIllness': function(id, regNo) {
        Cows.update({_id: id}, {
            $pull: {
                illnesses: {registerNo: regNo}
            }
        });
    },

    'farming.livestock.cows.updateStatus': function(data, id) {
        Cows.update({_id: id}, {$set: data});
    },

    'farming.livestock.cows.remove': function(id, reason) {
        var data = Cows.findOne({_id: id});

        RemovedCows.insert({
            identity: data.identity,
            date: moment().toDate(),
            reason: reason,
            data: JSON.stringify(data)
        });

        Cows.remove({_id: id});
    }
});