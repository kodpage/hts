Meteor.publish('farming.livestock.cow', function(id) {
    return Cows.find({_id: id});
});

Meteor.publish('farming.livestock.cow.stepAnalysis', function(rfid) {
    return StepCounts.find({
        rfid: rfid
    });
});

Meteor.publish('farming.livestock.cow.timedME', function(rfid, startDate, endDate) {
    return MilkData.find({
        rfid: rfid,
        date: {
            $gte: startDate,
            $lte: endDate
        }
    });
});

Meteor.publish('farming.livestock.bulls', function(id) {
    return Bulls.find({});
});
Meteor.publish('farming.livestock.sperms', function(id) {
    return Sperms.find({quantity: {$gt: 0}});
});

Meteor.publish('farming.livestock.groups', function() {
    return Groups.find({});
});