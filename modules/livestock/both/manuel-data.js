Schemas.manuelData = {};

Schemas.manuelData.main = new Schema({
    identity: {
        type: String,
        label: 'Kimlik'
    },
    date: {
        type: Date,
        label: 'Tarih',
        autoform: {
            type: 'bootstrap-datepicker'
        }
    },
    amount: {
        type: Number,
        label: 'Miktar (lt)',
        decimal: true
    }
});

ManuelData = Model.extend('manuel-data', {
    schema: Schemas.manuelData.main,
    permission: 'farming.livestock.manuel-data'
});

Tables.manuelData = new Tabular.Table({
    name: 'farming.livestock.manuel-data',
    collection: ManuelData,
    columns: [
        {data: 'identity', title: 'Kimlik'},
        {data: 'date', title: 'Tarih', render: function(data) {
            return moment(new Date(data)).format('DD-MM-YYYY HH:mm:ss');
        }},
        {data: 'amount', title: 'Miktar', render: function(d) {
            return d ? d.toFixed(2) : '';
        }}
    ],
    rowClick: function(data) {
        return '';
    }
});

if (Meteor.isServer) {
    ManuelData.before.insert(function(userId, doc) {
        var cow = Cows.findOne({identity: doc.identity}),
            date = doc.date;

        var startDate = moment(date).hour(0).minute(0).second(0).millisecond(0).toDate(),
            endDate = moment(date).hour(23).minute(59).second(59).millisecond(999).toDate();

        if (_.isObject(cow)) {
            var md = MilkData.findOne({rfid: cow.rfid, date: {$gte: startDate, $lt: endDate}});

            if (_.isObject(md)) {
                MilkData.update({_id: md._id}, {$set: {
                    amount: md.amount + doc.amount
                }});
            } else {
                MilkData.insert({
                    rfid: cow.rfid,
                    date: doc.date,
                    amount: doc.amount
                });
            }
        } else {
            throw new Meteor.Error('Sistemde bu kimliğe sahip inek mevcut değil');
        }
    });

    ManuelData.before.update(function(userId, doc, fieldNames, modifier) {
        var cow = Cows.findOne({identity: doc.identity}),
            newData = modifier.$set,
            date = newData.date;

        var startDate = moment(date).hour(0).minute(0).second(0).millisecond(0).toDate(),
            endDate = moment(date).hour(23).minute(59).second(59).millisecond(999).toDate();

        if (_.isObject(cow)) {
            var md = MilkData.findOne({rfid: cow.rfid, date: {$gte: startDate, $lt: endDate}});

            if (_.isObject(md)) {
                var diff = newData.amount - doc.amount;

                MilkData.update({_id: md._id}, {$set: {
                    amount: md.amount + diff
                }});
            } else {
                throw new Meteor.Error('Sistemde bu kimliğe sahip ineğin bu tarihte süt verisi mevcut değil');
            }
        } else {
            throw new Meteor.Error('Sistemde bu kimliğe sahip inek mevcut değil');
        }
    });

    ManuelData.before.remove(function (userId, doc) {
        var cow = Cows.findOne({identity: doc.identity}),
            date = doc.date;

        var startDate = moment(date).hour(0).minute(0).second(0).millisecond(0).toDate(),
            endDate = moment(date).hour(23).minute(59).second(59).millisecond(999).toDate();

        if (_.isObject(cow)) {
            var md = MilkData.findOne({rfid: cow.rfid, date: {$gte: startDate, $lt: endDate}});

            if (_.isObject(md)) {
                if (md.amount - doc.amount == 0) {
                    MilkData.remove({_id: md._id});
                } else {
                    MilkData.update({_id: md._id}, {$set: {
                        amount: md.amount - doc.amount
                    }});
                }
            }
        }
    });
}