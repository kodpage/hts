Schemas.milkData = new Schema({
    rfid: {
        type: String,
        label: 'RFID'
    },
    date: {
        type: Date,
        label: 'Tarih'
    },
    amount: {
        type: Number,
        label: 'Miktar (lt)',
        decimal: true
    }
});

MilkData = Model.extend('milk-data', {
    schema: Schemas.milkData
});

Schemas.milkDataSums = new Schema({
    date: {
        type: Date,
        label: 'Tarih'
    },
    amount: {
        type: Number,
        label: 'Miktar (lt)',
        decimal: true
    },
    rfids: {
        type: [String]
    }
});

MilkDataSums = Model.extend('milk-data-sums', {
    schema: Schemas.milkDataSums
});

if (Meteor.isServer) {
    MilkData.before.insert(function(userId, doc) {
        var date = doc.date,
            amount = doc.amount;

        var startDate = moment(date).hour(0).minute(0).second(0).millisecond(0).toDate(),
            endDate = moment(date).hour(23).minute(59).second(59).millisecond(999).toDate();

        var sum = MilkDataSums.findOne({date: {$gte: startDate, $lt: endDate}});

        if (_.isObject(sum)) {
            if (!_.contains(sum.identities, doc.identity)) {
                MilkDataSums.update({_id: sum._id}, {$addToSet: {rfids: doc.rfid}});
            }
            MilkDataSums.update({_id: sum._id}, {$set: {amount: (sum.amount + amount)}});
        } else {
            MilkDataSums.insert({
                date: date,
                amount: amount,
                rfids: [doc.rfid]
            });
        }
    });

    MilkData.before.update(function(userId, doc, fieldNames, modifier) {
        var data = modifier.$set,
            date = data.date,
            amount = data.amount - doc.amount;

        var startDate = moment(date).hour(0).minute(0).second(0).millisecond(0).toDate(),
            endDate = moment(date).hour(23).minute(59).second(59).millisecond(999).toDate();

        var sum = MilkDataSums.findOne({date: {$gte: startDate, $lt: endDate}});

        if (_.isObject(sum)) {
            MilkDataSums.update({_id: sum._id}, {$set: {amount: (sum.amount + amount)}});
        }
    });

    MilkData.before.remove(function (userId, doc) {
        var date = doc.date,
            amount = doc.amount;

        var startDate = moment(date).hour(0).minute(0).second(0).millisecond(0).toDate(),
            endDate = moment(date).hour(23).minute(59).second(59).millisecond(999).toDate();

        var sum = MilkDataSums.findOne({date: {$gte: startDate, $lt: endDate}});

        if (_.isObject(sum)) {
            if (sum.amount - doc.amount == 0) {
                MilkDataSums.remove({_id: sum._id});
            } else {
                if (!_.contains(sum.identities, doc.identity)) {
                    MilkDataSums.update({_id: sum._id}, {$pull: {rfids: doc.rfid}});
                }
                MilkDataSums.update({_id: sum._id}, {$set: {amount: (sum.amount - amount)}});
            }
        }
    });
}