Schemas.stepCounts = new Schema({
    identity: {
        type: String,
        label: 'Kimlik'
    },
    rfid: {
        type: String,
        label: 'RFID'
    },
    date: {
        type: Date,
        label: 'Tarih'
    },
    count: {
        type: Number,
        label: 'Adım'
    }
});

StepCounts = Model.extend('step-counts', {
    schema: Schemas.stepCounts
});