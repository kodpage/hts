Schemas.removedCows = {};

Schemas.removedCows.main = new Schema({
    // Must be unique
    identity: {
        type: String,
        label: 'Kimlik'
    },
    date: {
        type: Date,
        label: 'Tarih'
    },
    reason: {
        type: String,
        label: 'Çıkartılma Sebebi'
    },
    data: {
        type: String,
        optional: true
    }
});

RemovedCows = Model.extend('removed-cows', {
    schema: Schemas.removedCows.main,
    permission: 'farming.livestock.removed-cows'
});

Tables.removedCows = new Tabular.Table({
    name: 'farming.livestock.removed-cows',
    collection: RemovedCows,
    columns: [
        {data: 'identity', title: 'Kimlik'},
        {data: 'date', title: 'Tarih', render: function(date) {

            return moment(date).format('DD-MM-YYYY');
        }},
        {data: 'reason', title: 'Çıkartılma Sebebi'}
    ]
});