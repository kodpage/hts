Schemas.sperms = {};

Schemas.sperms.main = new Schema({
    name: {
        type: String,
        label: 'Ad',
        optional: true
    },
    identity: {
        type: String,
        label: 'Kimlik'
    },
    quantity: {
        type: Number,
        label: 'Adet',
        min: 1
    },
    acquisitionDate: {
        type: Date,
        label: 'Alım Tarihi',
        autoform: {
            type: 'bootstrap-datepicker'
        }
    },
    firm: {
        type: String,
        label: 'Firma',
        optional: true
    }
});

Sperms = Model.extend('sperms', {
    schema: Schemas.sperms.main,
    permission: 'farming.livestock.sperms'
});

Tables.sperms = new Tabular.Table({
    name: 'farming.livestock.sperms',
    collection: Sperms,
    columns: [
        {data: 'name', title: 'Ad'},
        {data: 'identity', title: 'Kimlik'},
        {data: 'quantity', title: 'Adet'},
        {data: 'acquisitionDate', title: 'Alım Tarihi', render: function(data) {
            return moment(new Date(data)).format('DD-MM-YYYY');
        }},
        {data: 'firm', title: 'Firma Adı'}
    ],
    rowClick: function(data) {
        return '';
    }
});