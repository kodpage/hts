Schemas.bulls = {};

Schemas.bulls.main = new Schema({
    name: {
        type: String,
        label: 'Ad',
        optional: true
    },
    identity: {
        type: String,
        label: 'Kimlik'
    },
    motherIdentity: {
        type: String,
        label: 'Anne Kimlik',
        optional: true
    },
    birthDate: {
        type: Date,
        label: 'Doğum Tarihi',
        autoform: {
            type: 'bootstrap-datepicker'
        }
    },
    acquisitionDate: {
        type: Date,
        label: 'Alım Tarihi',
        autoform: {
            type: 'bootstrap-datepicker'
        }
    },
    midwife: {
        type: String,
        label: 'Doğurtan',
        optional: true
    },
    firm: {
        type: String,
        label: 'Firma',
        optional: true
    }
});

Bulls = Model.extend('bulls', {
    schema: Schemas.bulls.main,
    permission: 'farming.livestock.bulls',
    helpers: {
        age: function() {
            var a = moment();
            var b = moment(this.birthDate);

            return a.diff(b, 'days');
        }
    }
});

Tables.bulls = new Tabular.Table({
    name: 'farming.livestock.bulls',
    collection: Bulls,
    columns: [
        {data: 'name', title: 'Ad'},
        {data: 'identity', title: 'Kimlik'},
        {data: 'age()', title: 'Yaş'},
        {data: 'birthDate', title: 'Doğum Tarihi', render: function(data) {
            return moment(new Date(data)).format('DD-MM-YYYY');
        }},
        {data: 'acquisitionDate', title: 'Alım Tarihi', render: function(data) {
            return moment(new Date(data)).format('DD-MM-YYYY');
        }},
        {data: 'firm', title: 'Firma Adı'}
    ],
    extraFields: ['motherIdentity', 'birthDate', 'midwife'],
    rowClick: function(data) {
        return '';
    }
});

if (Meteor.isServer) {
    Bulls.before.insert(function(userId, doc) {
        var motherIdentity = doc.motherIdentity;

        if (motherIdentity) {
            Cows.update({identity: motherIdentity}, {$addToSet: {childrenIdentities: doc.identity}});
        }
    });
}