Schemas.cows = {};

Schemas.cows.vaccinations = new Schema({
    registerNo: {
        type: Number,
        label: 'Kayıt No'
    },
    date: {
        type: Date,
        label: 'Tarih',
        autoform: {
            type: 'bootstrap-datepicker'
        }
    },
    name: {
        type: String,
        label: 'Ad'
    },
    type: {
        type: String,
        label: 'Tür'
    },
    appliedBy: {
        type: String,
        label: 'Uygulayan'
    },
    not: {
        type: String,
        label: 'Not'
    }
});

Schemas.cows.insemination = new Schema({
    registerNo: {
        type: Number,
        label: 'Kayıt No'
    },
    type: {
        type: String,
        label: 'Tohumlama Şekli',
        allowedValues: ['sperm', 'bull']
        //allowedValues: ['Sperm enjekte edildi', 'Boğaya çekildi']
    },
    identity: {
        type: String,
        label: 'Kimlik'
    },
    date: {
        type: Date,
        label: 'Tarih',
        autoform: {
            type: 'bootstrap-datepicker'
        }
    },
    appliedBy: {
        type: String,
        label: 'Tohumlayan'
    }
});

Schemas.cows.illnesses = new Schema({
    registerNo: {
        type: Number,
        label: 'Kayıt No'
    },
    date: {
        type: Date,
        label: 'Tarih',
        autoform: {
            type: 'bootstrap-datepicker'
        }
    },
    illness: {
        type: String,
        label: 'Hastalık'
    },
    application: {
        type: String,
        label: 'Uygulama'
    },
    result: {
        type: String,
        label: 'Sonuç'
    },
    note: {
        type: String,
        label: 'Not',
        optional: true
    }
});

Schemas.cows.main = new Schema({
    status: {
        type: [String],
        label: 'Güncel Durum',
        allowedValues: ['Boş', 'Fresh', 'Senkonize', 'Gebe', 'Hasta', 'Kuruda', 'Kasaplık'],
        autoform: {
            type: 'select2',
            multiple: true,
            style: '"width: 100%"',
            select2Options: {
                minimumResultsForSearch: Infinity
            }
        }
    },
    registerNo: {
        type: Number,
        label: 'Kayıt No'
    },
    // Must be unique
    identity: {
        type: String,
        label: 'Kimlik'
    },
    // Must be unique
    rfid: {
        type: String,
        label: 'RFID',
        optional: true
    },
    groupId: {
        type: String,
        label: 'Hayvan Grubu',
    },
    name: {
        type: String,
        label: 'Ad',
        optional: true
    },
    motherIdentity: {
        type: String,
        label: 'Anne Kimlik',
        optional: true
    },
    motherName: {
        type: String,
        label: 'Anne Adı',
        optional: true
    },
    fatherIdentity: {
        type: String,
        label: 'Baba Kimlik',
        optional: true
    },
    fatherName: {
        type: String,
        label: 'Baba Adı',
        optional: true
    },
    joinReason: {
        type: String,
        label: 'Sürüye Giriş Sebebi',
        allowedValues: ['Doğum', 'Satın Alma']
    },
    joinDate: {
        type: Date,
        label: 'Sürüye Giriş Tarihi',
        autoform: {
            type: 'bootstrap-datepicker'
        }
    },
    birthPlace: {
        type: String,
        label: 'Doğum Yeri',
        optional: true
    },
    birthDate: {
        type: Date,
        label: 'Doğum Tarihi',
        autoform: {
            type: 'bootstrap-datepicker'
        }
    },
    birthType: {
        type: String,
        label: 'Doğum Türü',
        allowedValues: ['Güç', 'Normal', 'Kendiliğinden', 'Sezeryan'],
        optional: true
    },
    birthWeight: {
        type: Number,
        label: 'Doğum Ağırlığı (kg)',
        optional: true
    },
    midwife: {
        type: String,
        label: 'Doğurtan',
        optional: true
    },
    note: {
        type: String,
        label: 'Not',
        optional: true
    },
    vaccinations: {
        type: [Schemas.cows.vaccinations],
        optional: true
    },
    insemination: {
        type: [Schemas.cows.insemination],
        optional: true
    },
    illnesses: {
        type: [Schemas.cows.illnesses],
        optional: true
    },
    childrenIdentities: {
        type: [String],
        defaultValue: [],
        optional: true
    }
});

Cows = Model.extend('cows', {
    schema: Schemas.cows.main,
    permission: 'farming.livestock.cows',
    helpers: {
        age: function() {
            var a = moment();
            var b = moment(this.birthDate);

            return a.diff(b, 'days');
        },

        group: function() {
            return Groups.findOne({_id: this.groupId}).name;
        },

        children: function() {
            var children = [];

            // Cows
            Cows.find({motherIdentity: this.identity}).forEach(function(cow) {
                children.push({
                    identity: cow.identity,
                    sex: 'Dişi',
                    midwife: cow.midwife,
                    age: cow.age(),
                    birthDate: cow.birthDate
                });
            });

            // Bulls
            Bulls.find({motherIdentity: this.identity}).forEach(function(bull) {
                children.push({
                    identity: bull.identity,
                    sex: 'Erkek',
                    midwife: bull.midwife,
                    age: bull.age(),
                    birthDate: bull.birthDate
                });
            });

            return _.sortBy(children, 'age').reverse();
        },

        hasChild: function() {
            return _.isArray(this.childrenIdentities) ? (this.childrenIdentities.length > 0) : false;
        },

        lactationCount: function() {
            return  _.isArray(this.childrenIdentities) ? this.childrenIdentities.length : 0;
        }
    }
});

if (Meteor.isServer) {
    Cows.before.insert(function(userId, doc) {
        var motherIdentity = doc.motherIdentity;

        if (motherIdentity) {
            Cows.update({identity: motherIdentity}, {$addToSet: {childrenIdentities: doc.identity}});
        }
    });
}

Tables.cows = new Tabular.Table({
    name: 'farming.livestock.cows',
    collection: Cows,
    columns: [
        {data: 'registerNo', title: 'Kayıt No'},
        {data: 'identity', title: 'Kimlik'},
        {data: 'rfid', title: 'RFID'},
        {data: 'name', title: 'Ad'},
        {data: 'birthDate', title: 'Yaş (Gün)', render: function(date) {
            var a = moment();
            var b = moment(date);

            return a.diff(b, 'days');
        }},
        {data: 'birthDate', title: 'Doğum Tarihi', render: function(date) {

            return moment(date).format('DD-MM-YYYY');
        }},
        {data: 'group()', title: 'Grup'},
        {
            data: 'status', title: 'Güncel Durum',
            tmpl: Meteor.isClient && Template.cowStatus
        }
    ],
    extraFields: ['groupId'],
    autoWidth: false
});