Schemas.groups = {};

Schemas.groups.main = new Schema({
    name: {
        type: String,
        label: 'Grup Adı'
    }
});

Groups = Model.extend('groups', {
    schema: Schemas.groups.main,
    permission: 'farming.livestock.groups'
});