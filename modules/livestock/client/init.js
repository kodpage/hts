Menu.set('farming.livestock', {
    title: 'Hayvanlar',
    name: 'farming.livestock',
    icon: 'wb-extension',
    order: 1
});

Menu.set('farming.livestock.cows', {
    title: 'İnek Yönetimi',
    name: 'farming.livestock.cows',
    order: 0
});

Menu.set('farming.livestock.groups', {
    title: 'Grup Yönetimi',
    name: 'farming.livestock.groups',
    order: 1
});

Menu.set('farming.livestock.bulls', {
    title: 'Boğa Yönetimi',
    name: 'farming.livestock.bulls',
    order: 2
});

Menu.set('farming.livestock.sperms', {
    title: 'Sperm Yönetimi',
    name: 'farming.livestock.sperms',
    order: 3
});

Menu.set('farming.livestock.removed-cows', {
    title: 'Çıkartılan İnekler',
    name: 'farming.livestock.removed-cows',
    order: 4
});

Menu.set('farming.livestock.manuel-data', {
    title: 'Manuel Veri',
    name: 'farming.livestock.manuel-data',
    order: 4
});
