Router.route('/farming/livestock/cows', {
    name: 'farming.livestock.cows',
    title: 'İnek Yönetimi',
    controller: CowsController
});
Router.route('/farming/livestock/cows/create', {
    name: 'farming.livestock.cows.create',
    title: 'İnek Oluştur',
    parent: 'farming.livestock.cows',
    controller: CowsCreateController
});
Router.route('/farming/livestock/cows/update/:_id/:section?/:stage?/:subId?', {
    name: 'farming.livestock.cows.update',
    title: 'İneği Güncelle',
    parent: 'farming.livestock.cows',
    controller: CowsUpdateController
});

Router.route('/farming/livestock/groups', {
    name: 'farming.livestock.groups',
    title: 'Grup Yönetimi',
    controller: GroupsController
});

Router.route('/farming/livestock/bulls', {
    name: 'farming.livestock.bulls',
    title: 'Boğa Yönetimi',
    controller: BullsController
});

Router.route('/farming/livestock/sperms', {
    name: 'farming.livestock.sperms',
    title: 'Sperm Yönetimi',
    controller: SpermsController
});

Router.route('/farming/livestock/removed-cows', {
    name: 'farming.livestock.removed-cows',
    title: 'Çıkartılan İnekler',
    controller: RemovedCowsController
});

Router.route('/farming/livestock/manuel-data', {
    name: 'farming.livestock.manuel-data',
    title: 'Manuel Data',
    controller: ManuelDataController
});