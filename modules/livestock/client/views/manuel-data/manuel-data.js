Template.manuelDataPage.helpers({
    table: function() {
        return Tables.manuelData
    },

    createAction: function() {
        return function() {
            bootbox.dialog({
                title: 'Yeni veri ekle',
                message: ' '
            });

            Blaze.renderWithData(Template.manuelDataCreate, null, $('.bootbox-body')[0]);
        };
    },

    updateAction: function() {
        return function(id) {
            var data = ManuelData.findOne({_id: id});

            bootbox.dialog({
                title: 'Veriyi düzenle',
                message: ' '
            });

            Blaze.renderWithData(Template.manuelDataUpdate, data, $('.bootbox-body')[0]);
        };
    },

    deleteAction: function() {
        return {
            msg: 'Seçilen verileri silmek istediğinize emin misiniz?',
            callback: function(isConfirmed) {
                if (isConfirmed) {
                    var data = Session.get('farming.livestock.manuel-data.selectedRows');

                    for (var i = 0; i < data.length; i++) {
                        ManuelData.remove({_id: data[i]._id});
                    }
                }
            }
        };
    }
});