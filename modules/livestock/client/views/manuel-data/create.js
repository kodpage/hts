AutoForm.hooks({
    createManuelDataForm: {
        onSuccess: function(formType, result) {
            bootbox.hideAll();
            Notify.success('Veri başarıyla eklendi.');
            Logs.info('Yeni manuel veri eklendi.');
        }
    }
});