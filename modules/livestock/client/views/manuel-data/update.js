AutoForm.hooks({
    updateManuelDataForm: {
        onSuccess: function(formType, result) {
            bootbox.hideAll();
            Notify.success('Veri başarıyla güncellendi.');

            Logs.info('Manuel veri güncellendi.');
        }
    }
});