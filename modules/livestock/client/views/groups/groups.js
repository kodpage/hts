Session.set('farming.livestock.groups.selected', null);
Session.set('farming.livestock.groups.counts', null);

Template.groupsPage.helpers({
    groups: function() {
        return Groups.find();
    },

    count: function(id) {
        var counts = Session.get('farming.livestock.groups.counts');

        return counts ? counts[id] : null;
    },

    table: function() {
        var selected = Session.get('farming.livestock.groups.selected');

        if (selected) {
            return new Tabular.Table({
                name: 'farming.livestock.cows',
                collection: Cows,
                columns: [
                    {data: 'registerNo', title: 'Kayıt No'},
                    {data: 'identity', title: 'Kimlik'},
                    {data: 'rfid', title: 'RFID'},
                    {data: 'age()', title: 'Yaş (Gün)'}
                ],
                extraFields: ['birthDate']
            });
        }
    },

    selector: function() {
        var selected = Session.get('farming.livestock.groups.selected');

        return {groupId: selected};
    },

    isShown: function(action) {
        return (Meteor.user() && Meteor.user().hasPermission('farming.livestock.groups.' + action));
    }
});

Template.groupsPage.events({
    'click .list-group-item': function(e, t) {
        var $this = t.$(e.target);

        if (!$this.is('.list-group-item')) {
            $this = $this.closest('.list-group-item');
        }

        var id = $this.data('id');

        if (id) {
            t.$('.list-group-item').removeClass('active');
            Session.set('farming.livestock.groups.selected', $this.data('id'));
            $this.addClass('active');
        }
    },

    'click .add-group': function(e, t) {
        var $listGroup = t.$('.list-group');

        var o = '<li class="list-group-item fake-item">' +
            '<div class="input-group">' +
            '<input type="text" class="form-control input-sm" name="" placeholder="Group Adı">' +
            '<span class="input-group-btn">' +
            '<button type="button" class="btn btn-primary btn-sm add-group-save">Kaydet</button>' +
            '</span>' +
            '</div>' +
            '</li>';

        if (t.$('.fake-item').length < 1) {
            $listGroup.append(o);

            t.$('.list-group-item').removeClass('active');
            t.$('.fake-item').addClass('active').find('input').focus();
        }
    },

    'click .add-group-save': function(e, t) {
        var $input = t.$('.fake-item input'),
            value = $input.val();

        if (value) {
            Groups.insert({name: value});
            t.$('.fake-item').remove();
            Notify.success('Yeni grup başarıyla oluşturuldu.');
        } else {
            Alert.error('Hata', 'Group adı girilmelidir!');
        }

        Logs.info('Yeni grup oluşturuldu.');
    },

    'click .update-group': function(e, t) {
        var selected = Session.get('farming.livestock.groups.selected'),
            $item = t.$('.list-group-item[data-id="' + selected + '"]'),
            orgName = $item.find('.item-name').text(),
            count = $item.find('.badge-success').text();

        $item.html('<input type="text" data-id="' + selected + '" data-count="' + count + '" data-name="' + orgName + '" class="form-control input-sm update-input" name="" value="' + orgName + '">');
        t.$('.update-input').focus().select();
    },

    'focusout .update-input, keypress .update-input': function(e, t) {
        if ((e.type == 'keypress' && e.keyCode == 13) || e.type == 'focusout') {
            var $input = t.$('.update-input'),
                $item = $input.closest('.list-group-item'),
                groupId = $input.data('id'),
                orgName = $input.data('name'),
                count = $input.data('count'),
                value = $input.val();

            if (value) {
                Groups.update({_id: groupId}, {$set: {name: value}});
                Notify.success('Grup adı başarıyla güncellendi.');
                Logs.info('Grup bilgileri güncellendi.');

                var o = '<span class="badge badge-success" style="display: block">' + count +'</span>' +
                        '<span class="item-name">' + value + '</span>';
                $item.html(o);
            } else {
                var o = '<span class="badge badge-success" style="display: block">' + count +'</span>' +
                        '<span class="item-name">' + orgName + '</span>';
                $item.html(o);
                Alert.error('Hata', 'Group adı girilmelidir!');
            }
        }
    },

    'click .delete-group': function(e, t) {
        var selected = Session.get('farming.livestock.groups.selected'),
            $item = t.$('.list-group-item[data-id="' + selected + '"]'),
            count = $item.find('.badge-success').text();

        if (count > 0) {
            Alert.error('Hata', 'Bu grupta yer alan hayvanlar mevcut. Grubu silmek için bu grupta yer alan hayvanları başka bir gruba taşıyınız!');
        } else {
            Groups.remove({_id: selected});
            t.$('.list-group-item:first').trigger('click');
            Notify.success('Grup başarıyla silindi!');
            Logs.info('Grup silindi.');
        }
    }

});

Template.groupsPage.onRendered(function() {
    //this.$('.list-group-item:first').trigger('click');

    this.autorun(function() {
        Session.set('farming.livestock.groups.counts', ReactiveMethod.call('farming.livestock.cows.getGroupCowCount'));
    });
});