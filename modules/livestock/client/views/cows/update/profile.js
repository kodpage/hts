Template.cowsUpdateProfile.helpers({
    report: function() {
        return ReactiveMethod.call('farming.livestock.cows.report', this._id);
    }
});

Template.cowsUpdateProfile.events({
    'click .btn-update-status': function() {
        bootbox.dialog({
            title: 'Durumu Güncelle',
            message: ' '
        });

        Blaze.renderWithData(Template.cowsUpdateProfileStatusUpdate, this, $('.bootbox-body')[0]);
    },

    'click .btn-remove': function() {
        bootbox.dialog({
            title: 'Sürüden Çıkart',
            message: ' '
        });

        Blaze.renderWithData(Template.cowsUpdateProfileRemove, this, $('.bootbox-body')[0]);
    }
});