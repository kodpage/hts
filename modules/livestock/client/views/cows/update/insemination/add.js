AutoForm.hooks({
    cowsUpdateInseminationAddForm: {
        onSubmit: function(fields) {
            var id = Router.current().params._id,
                insemination = Cows.findOne({_id: id}).insemination;

            if (insemination && insemination.length > 0) {
                fields.registerNo = parseInt(insemination[insemination.length - 1].registerNo) + 1;
            } else {
                fields.registerNo = 1;
            }

            Cows.update({_id: id}, {$addToSet: {insemination: fields}});

            // Decrease sperm quantity if type is sperm
            if (fields.type == 'sperm') {
                Meteor.call('farming.livestock.cows.update.decreaseSpermQuantity', fields);
            }

            bootbox.hideAll();

            Notify.success('Tohumlama girdisi başarıyla oluşturuldu.');

            var identity = Cows.findOne({_id: id}).identity;
            Logs.info(identity + ' kimliğine sahip ineğe tohumlama girdisi eklendi.');

            return false;
        }
    }
});

Template.cowsUpdateInseminationAdd.helpers({
    schema: function() {
        return new Schema({
            type: {
                type: String,
                label: 'Tohumlama Şekli',
                allowedValues: ['sperm', 'bull']
            },
            identity: {
                type: String,
                label: 'Kimlik',
                autoform: {
                    type: 'select2',
                    select2Options: {
                        templateResult: function(item) {
                            if (!item.id) {
                                return item.text;
                            }

                            var data = JSON.parse(item.text),
                                o = '<div><strong>' + data.type + '</strong></div>';

                            if (data.type == 'Sperm')
                                o += '<div>' + data.name + ' - ' + data.identity + ' - ' + data.quantity + '</div>';
                            else
                                o += '<div>' + data.name + ' - ' + data.identity + '</div>';

                            return $(o);
                        },

                        templateSelection: function(item) {
                            var data = JSON.parse(item.text);

                            return data.identity;
                        }
                    }
                }
            },
            date: {
                type: Date,
                label: 'Tarih',
                autoform: {
                    type: 'bootstrap-datepicker'
                }
            },
            appliedBy: {
                type: String,
                label: 'Tohumlayan'
            }
        });
    },

    typeOtions: function() {
        return [
            {label: 'Boğa', value: 'bull'},
            {label: 'Sperm', value: 'sperm'}
        ];
    },

    identityOptions: function() {
        var suggestions = [];

        Sperms.find({quantity: {$gt: 0}}).forEach(function(sperm) {
            suggestions.push({
                name: sperm.name,
                identity: sperm.identity,
                quantity: sperm.quantity,
                type: 'Sperm'
            });
        });

        Bulls.find({}).forEach(function(bull) {
            suggestions.push({
                name: bull.name,
                identity: bull.identity,
                type: 'Boğa'
            });
        });

        return suggestions.map(function(suggestion) {
            var data = {};

            data.value = suggestion.identity;
            data.label = JSON.stringify(suggestion);

            return data;
        });
    }
});