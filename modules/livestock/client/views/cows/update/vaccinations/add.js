AutoForm.hooks({
    cowsUpdateVaccinationsAddForm: {
        onSubmit: function(fields) {
            var id = Router.current().params._id,
                vaccinations = Cows.findOne({_id: id}).vaccinations;

            if (vaccinations && vaccinations.length > 0) {
                fields.registerNo = parseInt(vaccinations[vaccinations.length - 1].registerNo) + 1;
            } else {
                fields.registerNo = 1;
            }

            Cows.update({_id: id}, {$addToSet: {vaccinations: fields}});
            bootbox.hideAll();

            Notify.success('Aşılama girdisi başarıyla oluşturuldu.');

            var identity = Cows.findOne({_id: id}).identity;
            Logs.info(identity + ' kimliğine sahip ineğe aşılama girdisi eklendi.');

            return false;
        }
    }
});

Template.cowsUpdateVaccinationsAdd.helpers({
    schema: function() {
        return new Schema({
            name: {
                type: String,
                label: 'Ad'
            },
            type: {
                type: String,
                label: 'Tür'
            },
            date: {
                type: Date,
                label: 'Tarih',
                autoform: {
                    type: 'bootstrap-datepicker'
                }
            },
            appliedBy: {
                type: String,
                label: 'Uygulayan'
            },
            not: {
                type: String,
                label: 'Not'
            }
        });
    }
});