AutoForm.hooks({
    cowsUpdateVaccinationsUpdateForm: {
        onSubmit: function(fields) {
            var id = Router.current().params._id,
                regNo = Session.get('farming.livestock.cows.update.selectedVaccination');

            Meteor.call('farming.livestock.cows.update.updateVaccination', id, regNo, fields, function() {
                Session.set('farming.livestock.cows.update.selectedVaccination', 'no');
                bootbox.hideAll();

                Notify.success('Aşılama girdisi başarıyla güncellendi.');

                var identity = Cows.findOne({_id: id}).identity;
                Logs.info(identity + ' kimliğine sahip ineğin aşı girdisi güncellendi.');
            });

            return false;
        }
    }
});

Template.cowsUpdateVaccinationsUpdate.helpers({
    doc: function() {
        var id = Router.current().params._id,
            vaccinations = Cows.findOne({_id: id}).vaccinations,
            regNo = Session.get('farming.livestock.cows.update.selectedVaccination');

        return _.findWhere(vaccinations, {registerNo: regNo});
    },
    schema: function() {
        return new Schema({
            name: {
                type: String,
                label: 'Ad'
            },
            type: {
                type: String,
                label: 'Tür'
            },
            date: {
                type: Date,
                label: 'Tarih',
                autoform: {
                    type: 'bootstrap-datepicker'
                }
            },
            appliedBy: {
                type: String,
                label: 'Uygulayan'
            },
            not: {
                type: String,
                label: 'Not'
            }
        });
    }
});