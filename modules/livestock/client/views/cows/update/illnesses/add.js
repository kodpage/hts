AutoForm.hooks({
    cowsUpdateIllnessesAddForm: {
        onSubmit: function(fields) {
            var id = Router.current().params._id,
                illnesses = Cows.findOne({_id: id}).illnesses;

            if (illnesses && illnesses.length > 0) {
                fields.registerNo = parseInt(illnesses[illnesses.length - 1].registerNo) + 1;
            } else {
                fields.registerNo = 1;
            }

            Cows.update({_id: id}, {$addToSet: {illnesses: fields}});
            bootbox.hideAll();

            Notify.success('Hastalık / tedavi girdisi başarıyla oluşturuldu.');

            var identity = Cows.findOne({_id: id}).identity;
            Logs.info(identity + ' kimliğine sahip ineğe hastalık girdisi eklendi.');

            return false;
        }
    }
});

Template.cowsUpdateIllnessesAdd.helpers({
    schema: function() {
        return new Schema({
            date: {
                type: Date,
                label: 'Tarih',
                autoform: {
                    type: 'bootstrap-datepicker'
                }
            },
            illness: {
                type: String,
                label: 'Hastalık'
            },
            application: {
                type: String,
                label: 'Uygulama'
            },
            result: {
                type: String,
                label: 'Sonuç'
            },
            note: {
                type: String,
                label: 'Not'
            }
        });
    }
});