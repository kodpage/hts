AutoForm.hooks({
    cowsUpdateIllnessesUpdateForm: {
        onSubmit: function(fields) {
            var id = Router.current().params._id,
                regNo = Session.get('farming.livestock.cows.update.selectedIllness');

            Meteor.call('farming.livestock.cows.update.updateIllness', id, regNo, fields, function() {
                Session.set('farming.livestock.cows.update.selectedIllness', 'no');
                bootbox.hideAll();

                Notify.success('Hastalık / tedavi girdisi başarıyla güncellendi.');

                var identity = Cows.findOne({_id: id}).identity;
                Logs.info(identity + ' kimliğine sahip ineğin hastalık girdisi güncellendi.');
            });

            return false;
        }
    }
});

Template.cowsUpdateIllnessesUpdate.helpers({
    doc: function() {
        var id = Router.current().params._id,
            illnesses = Cows.findOne({_id: id}).illnesses,
            regNo = Session.get('farming.livestock.cows.update.selectedIllness');

        return _.findWhere(illnesses, {registerNo: regNo});
    },
    schema: function() {
        return new Schema({
            date: {
                type: Date,
                label: 'Tarih',
                autoform: {
                    type: 'bootstrap-datepicker'
                }
            },
            illness: {
                type: String,
                label: 'Hastalık'
            },
            application: {
                type: String,
                label: 'Uygulama'
            },
            result: {
                type: String,
                label: 'Sonuç'
            },
            note: {
                type: String,
                label: 'Not'
            }
        });
    }
});