AutoForm.hooks({
    updateCowsForm: {
        before: {
            insert: function(doc) {
                if (Cows.find({identity: doc.identity}).count() > 0) {
                    Cows.simpleSchema().namedContext('updateCowsForm').addInvalidKeys([{
                        name: 'identity',
                        type: "notUnique"
                    }]);
                    return false;
                }

                if (Cows.find({rfid: doc.rfid}).count() > 0) {
                    Cows.simpleSchema().namedContext('updateCowsForm').addInvalidKeys([{
                        name: 'rfid',
                        type: "notUnique"
                    }]);
                    return false;
                }

                var a = moment(),
                    b = moment(doc.birthDate),
                    c = a.diff(b, 'days');
                if (c < 0) {
                    Cows.simpleSchema().namedContext('updateCowsForm').addInvalidKeys([{
                        name: 'birthDate',
                        type: "badDate"
                    }]);
                }

                a = moment();
                b = moment(doc.joinDate);
                c = a.diff(b, 'days');
                if (c < 0) {
                    Cows.simpleSchema().namedContext('updateCowsForm').addInvalidKeys([{
                        name: 'joinDate',
                        type: "badDate"
                    }]);
                }

                Logs.info(doc.identity + ' kimliğine sahip ineğin bilgileri güncellendi.');

                return doc;
            }
        },
        onSuccess: function(formType, result) {
            Notify.success('Hayvan bilgileri başarıya güncellendi.');
        }
    }
});

Template.cowsUpdateInfo.helpers({
    groups: function() {
        return Groups.find().fetch().map(function(group) {
            return {label: group.name, value: group._id};
        });
    },

    statusOptions: function() {
        return [
            {label: 'Boş', value: 'Boş'},
            {label: 'Gebe', value: 'Gebe'},
            {label: 'Hasta', value: 'Hasta'},
            {label: 'Fresh', value: 'Fresh'},
            {label: 'Senkonize', value: 'Senkonize'},
            {label: 'Kuruda', value: 'Kuruda'},
            {label: 'Kasaplık', value: 'Kasaplık'}
        ];
    }
});