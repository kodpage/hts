Template.cowsUpdateTimedMEDetails.helpers({
    milkingData: function() {
        var startDate = Session.get('farming.livestock.cows.timedME.startDate'),
            endDate = Session.get('farming.livestock.cows.timedME.endDate'),
            rfid = this.rfid,
            md = null,
            data = {};

        data.days = 0;
        data.total = 0;
        data.dailyMilked = 0;
        data.monthlyMilked = 0;
        data.yearlyMilked = 0;

        if (startDate && endDate) {
            md =  MilkData.find({
                rfid: rfid,
                date: {
                    $gte: startDate,
                    $lte: endDate
                }
            }, {sort: {date: 1}});

            data.days = md.count();
            data.total = 0;

            md.forEach(function(d) {
                data.total += d.amount;
            });

            data.dailyMilked = data.total / data.days;
            if (isNaN(data.dailyMilked))
                data.dailyMilked = 0;

            if (data.days >= 30) {
                var monthCount = data.days / 30;

                data.monthlyMilked = data.total / monthCount;
            } else {
                data.monthlyMilked = null;
            }

            if (data.days >= 365) {
                var yearCount = parseInt(data.days / 365);

                data.yearlyMilked = data.total / yearCount;
            } else {
                data.yearlyMilked = null;
            }

            data.days += ' Gün';
            data.total = data.total.toFixed(2) + ' Lt';
            data.dailyMilked = data.dailyMilked.toFixed(2) + ' Lt';
            data.monthlyMilked = data.monthlyMilked && data.monthlyMilked.toFixed(2) + ' Lt';
            data.yearlyMilked = data.yearlyMilked && data.yearlyMilked.toFixed(2) + ' Lt';
        }

        return data;
    }
});