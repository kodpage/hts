Template.cowsUpdateTimedMEGraph.helpers({});

Template.cowsUpdateTimedMEGraph.onRendered(function() {
    var $graphPanel = $('div.graph-panel'),
        graph = Morris.Line({
            element: 'timedMEGraph',
            data: [],
            xkey: 'date',
            ykeys: ['amount'],
            labels: ['Miktar'],
            //resize: true,
            pointSize: 1,
            smooth: true,
            gridTextColor: '#474e54',
            gridLineColor: '#eef0f2',
            goalLineColors: '#e3e6ea',
            gridTextFamily: $.configs.get('site', 'fontFamily'),
            gridTextWeight: '300',
            //numLines: 9,
            gridtextSize: 14,
            lineWidth: 1,
            lineColors: [$.colors('primary', 700)],
            postUnits: ' Lt',
            dateFormat: function(x) {
                return moment(x).format('DD.MM.YYYY');
            },
            yLabelFormat: function (y) {
                return y.toFixed(2) + ' Lt';
            }
        });

    this.autorun(function() {
        var startDate = Session.get('farming.livestock.cows.timedME.startDate'),
            endDate = Session.get('farming.livestock.cows.timedME.endDate'),
            rfid = Template.currentData().rfid;

        if (startDate || endDate) {
            $graphPanel.block();

            Meteor.subscribe('farming.livestock.cow.timedME', rfid, startDate, endDate, function() {
                var data = [],
                    md = MilkData.find({
                        rfid: rfid,
                        date: {
                            $gte: startDate,
                            $lte: endDate
                        }
                    }, {sort: {date: 1}}).fetch();

                for (var i = 0; i < md.length; i++) {
                    var d = md[i],
                        nextD = md[i + 1];

                    data.push({
                        amount: d.amount,
                        date: moment(d.date).format('YYYY-MM-DD')
                    });

                    if (nextD) {
                        if (moment(nextD.date).diff(moment(d.date), 'days') > 1) {
                            var newDate = null;

                            do {
                                newDate = (newDate) ? newDate.add(1, 'days') : moment(d.date).add(1, 'days');

                                data.push({
                                    amount: 0.00,
                                    date: newDate.format('YYYY-MM-DD')
                                });
                            } while (moment(nextD.date).diff(newDate, 'days') > 0);
                        }
                    } else if (moment().diff(moment(d.date), 'days') > 0) {
                        newDate = null;

                        do {
                            newDate = (newDate) ? newDate.add(1, 'days') : moment(d.date).add(1, 'days');

                            data.push({
                                amount: 0.00,
                                date: newDate.format('YYYY-MM-DD')
                            });
                        } while (moment().diff(newDate, 'days') > 1);
                    }
                }

                graph.setData(data);

                $graphPanel.unblock();

                data = null;
                md = null;
            });
        }
    });

});