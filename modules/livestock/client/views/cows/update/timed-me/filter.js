Template.cowsUpdateTimedMEFilters.events({
    'click .excel-btn': function() {
        var $graphPanel = $('div.graph-panel'),
            startDate = Session.get('farming.livestock.cows.timedME.startDate'),
            endDate = Session.get('farming.livestock.cows.timedME.endDate'),
            rfid = this.rfid,
            self = this;

        var base64ToBlob = function(base64String) {
            var byteArray, byteCharacters, byteNumbers, i;

            byteCharacters = atob(base64String);
            byteNumbers = new Array(byteCharacters.length);
            i = 0;

            while (i < byteCharacters.length) {
                byteNumbers[i] = byteCharacters.charCodeAt(i);
                i++;
            }

            byteArray = new Uint8Array(byteNumbers);

            return new Blob([byteArray], {type: 'application/vnd.openxmlformats'});
        };

        if (startDate && endDate && rfid) {
            $graphPanel.block();

            Meteor.call('farming.livestock.cows.exportMilkingData', rfid, startDate, endDate, function(error, data) {
                if (error) {
                    Alert.error('Hata Oluştu', error.reason);
                } else {
                    var blob = base64ToBlob(data);

                    saveAs(blob, 'sut-verimi.xlsx');

                    Logs.info(self.identity + ' kimliğine sahip ineğin zamansal süt verimleri excele aktarıldı.');

                    $graphPanel.unblock();
                }
            });
        }
    }
});

Template.cowsUpdateTimedMEFilters.onRendered(function() {
    var $dateRange = this.$('.input-daterange');

    Meteor.call('farming.livestock.cows.report', Router.current().params._id, function(error, report) {
        if (report && _.isObject(report)) {
            $dateRange.datepicker({
                endDate: '+1d',
                startDate: '-' + moment().diff(moment(report.firstDeliveryDate), 'days') + 'd',
                language: 'tr',
                todayHighlight: true
            });

            $dateRange.find('input[name="start"]').datepicker('setDate', moment(report.lastDeliveryDate).format('DD.MM.YYYY'));
            $dateRange.find('input[name="end"]').datepicker('setDate', moment().format('DD.MM.YYYY'));

            $dateRange.datepicker().on('changeDate', function(e) {
                if ($(e.target).is('input[name="end"]')) {
                    Session.set('farming.livestock.cows.timedME.endDate', moment(e.date).toDate());
                } else {
                    Session.set('farming.livestock.cows.timedME.startDate', moment(e.date).toDate());
                }
            });

            Session.set('farming.livestock.cows.timedME.startDate', moment(report.lastDeliveryDate).toDate());
            Session.set('farming.livestock.cows.timedME.endDate', moment().toDate());

            $dateRange.find('input[name="start"], input[name="end"]').datepicker().on('show', function() {
                $('.datepicker.datepicker-dropdown').css('z-index', 1400);
            });
        }
    });
});