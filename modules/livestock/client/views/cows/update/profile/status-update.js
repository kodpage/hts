AutoForm.hooks({
    cowsUpdateProfileStatusUpdateForm: {
        onSubmit: function(fields) {
            var id = Router.current().params._id;

            Meteor.call('farming.livestock.cows.updateStatus', fields, id, function() {
                bootbox.hideAll();
                Notify.success('İneğin durumu başarıyla güncellendi.');
                var identity = Cows.findOne({_id: id}).identity;
                Logs.info(identity + ' kimliğine sahip ineğin durumu güncellendi.');
            });

            return false;
        }
    }
});

Template.cowsUpdateProfileStatusUpdate.helpers({
    schema: function() {
        return new Schema({
            status: {
                type: [String],
                label: 'Güncel Durum',
                allowedValues: ['Boş', 'Fresh', 'Senkonize', 'Gebe', 'Hasta', 'Kuruda', 'Kasaplık'],
                autoform: {
                    type: 'select2',
                    multiple: true,
                    style: '"width: 100%"',
                    select2Options: {
                        minimumResultsForSearch: Infinity
                    }
                }
            }
        });
    },

    statusOptions: function() {
        return [
            {label: 'Boş', value: 'Boş'},
            {label: 'Gebe', value: 'Gebe'},
            {label: 'Hasta', value: 'Hasta'},
            {label: 'Fresh', value: 'Fresh'},
            {label: 'Senkonize', value: 'Senkonize'},
            {label: 'Kuruda', value: 'Kuruda'},
            {label: 'Kasaplık', value: 'Kasaplık'}
        ];
    }
});