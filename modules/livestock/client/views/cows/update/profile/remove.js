AutoForm.hooks({
    cowsUpdateProfileRemoveForm: {
        onSubmit: function(fields) {
            var id = Router.current().params._id;

            Meteor.call('farming.livestock.cows.remove', id, fields.reason, function() {
                bootbox.hideAll();
                Router.go('farming.livestock.cows');
                Notify.success('İnek başarıyla sürüden çıkartıldı.');

                var identity = Cows.findOne({_id: id}).identity;
                Logs.info(identity + ' kimliğine sahip inek sürüden çıkartıldı.');
            });

            return false;
        }
    }
});

Template.cowsUpdateProfileRemove.helpers({
    schema: function() {
        return new Schema({
            reason: {
                type: String,
                label: 'Sürüden çıkartma sebebi'
            }
        });
    }
});