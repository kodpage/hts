Template.cowsUpdateIllnesses.helpers({
    isDisabled: function() {
        var selected = Session.get('farming.livestock.cows.update.selectedIllness');

        return (selected == null || selected == 'no') ? 'disabled': '';
    },

    illnessesData: function() {
        return _.sortBy(this.illnesses, 'date');
    }
});

Template.cowsUpdateIllnesses.events({
    'click .btn-add': function(e, t) {
        bootbox.dialog({
            title: 'Yeni hastalık / tedavi girdisi ekle',
            message: ' '
        });

        Blaze.renderWithData(Template.cowsUpdateIllnessesAdd, null, $('.bootbox-body')[0]);
    },

    'click tr.item': function(e, t) {
        var $this = t.$(e.target);

        if (! $this.is('tr')) {
            $this = $this.closest('tr');
        }

        Session.set('farming.livestock.cows.update.selectedIllness', this.registerNo);

        t.$('tr.item').removeClass('selected');
        $this.addClass('selected');
    },

    'click .btn-update:not(.disabled)': function(e, t) {
        bootbox.dialog({
            title: 'Hastalık / tedavi girdisini düzenle',
            message: ' '
        });

        Blaze.renderWithData(Template.cowsUpdateIllnessesUpdate, null, $('.bootbox-body')[0]);
    },

    'click .btn-delete:not(.disabled)': function(e, t) {
        var id = Router.current().params._id,
            regNo = Session.get('farming.livestock.cows.update.selectedIllness');

        Alert.confirm('Onayla!', 'Hastalık / tedavi girdisini silmek istediğinizden emin misiniz?', function(res) {
            if (res) {
                Meteor.call('farming.livestock.cows.update.deleteIllness', id, regNo, function() {
                    Session.set('farming.livestock.cows.update.selectedIllness', 'no');

                    Notify.success('Hastalık / tedavi girdisi başarıyla silindi.');
                });
            }
        });
    }
});

Template.cowsUpdateIllnesses.onRendered(function() {
    Session.set('farming.livestock.cows.update.selectedIllness', null);

    this.autorun(function() {
        var selected = Session.get('farming.livestock.cows.update.selectedIllness');

        if (selected == 'no'){
            $('tr.item').removeClass('selected');
        }
    });
});