Template.cowsUpdateFertility.helpers({
    illnessesInfo: function() {
        var report = ReactiveMethod.call('farming.livestock.cows.report', this._id),
            data = [],
            lactation = 1,
            self = this;

        // Order insemination
        this.insemination = _.sortBy(this.insemination, 'date');

        if (report && _.isObject(report)) {
            _.each(report.children, function(child, index) {
                // Find the insemination that results
                var insemination = [];

                if (index > 0) {
                    _.each(self.insemination, function(ins) {
                        if (
                            moment(child.birthDate).subtract(Settings.get('farming.pregnancyDuration'), 'days').diff(moment(ins.date), 'days') >= 0 &&
                            moment(ins.date).diff(moment(report.children[index - 1].birthDate).subtract(Settings.get('farming.pregnancyDuration'), 'days'), 'days') >= 0
                        ) {
                            insemination.push(ins);
                        }
                    });
                } else {
                    _.each(self.insemination, function(ins) {
                        if (moment(child.birthDate).subtract(Settings.get('farming.pregnancyDuration'), 'days').diff(moment(ins.date), 'days') >= 0) {
                            insemination.push(ins);
                        }
                    });
                }

                var lastInsemination = insemination[insemination.length - 1];
                if (lastInsemination) {
                    data.push({
                        lactation: lactation,
                        inseminationRegisterNo: lastInsemination.registerNo,
                        inseminationDate: moment(lastInsemination.date).format('DD-MM-YYYY'),
                        inseminationIdentity: lastInsemination.identity,
                        inseminationCount: insemination.length,
                        inseminationType: (lastInsemination.type == 'sperm') ? 'Sperm' : 'Boğa',
                        inseminationAppliedBy: lastInsemination.appliedBy
                    });
                }

                lactation++;
            });
        }

        return data;
    },

    fertilities: function() {
        var report = ReactiveMethod.call('farming.livestock.cows.report', this._id),
            data = [],
            lactation = 1,
            self = this;

        // Order insemination
        this.insemination = _.sortBy(this.insemination, 'date');

        if (report && _.isObject(report)) {
            _.each(report.children, function(child, index) {
                var insemination = [];

                if (index > 0) {
                    _.each(self.insemination, function(ins) {
                        if (
                            moment(child.birthDate).subtract(Settings.get('farming.pregnancyDuration'), 'days').diff(moment(ins.date), 'days') >= 0 &&
                            moment(ins.date).diff(moment(report.children[index - 1].birthDate).subtract(Settings.get('farming.pregnancyDuration'), 'days'), 'days') >= 0
                        ) {
                            insemination.push(ins);
                        }
                    });
                } else {
                    _.each(self.insemination, function(ins) {
                        if (moment(child.birthDate).subtract(Settings.get('farming.pregnancyDuration'), 'days').diff(moment(ins.date), 'days') >= 0) {
                            insemination.push(ins);
                        }
                    });
                }

                var pregnancyDuration = '';
                if (index > 0) {
                    pregnancyDuration = moment(child.birthDate).diff(moment(report.children[index - 1].birthDate), 'days');
                }

                var lastInsemination = insemination[insemination.length - 1];
                if (lastInsemination) {
                    data.push({
                        lactation: lactation,
                        inseminationInterval: moment(lastInsemination.date).diff(moment(insemination[0].date), 'days'),
                        pregnancyDuration: pregnancyDuration,
                        inseminationCount: insemination.length,
                        inseminationType: (lastInsemination.type == 'sperm') ? 'Sperm' : 'Boğa',
                        inseminationAppliedBy: lastInsemination.appliedBy,
                        efficiency: ((1 / insemination.length) * 100).toFixed(2) + '%'
                    });
                }

                lactation++;
            });
        }

        return data;
    }
});