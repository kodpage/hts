Template.cowsUpdateVaccinations.helpers({
    isDisabled: function() {
        var selected = Session.get('farming.livestock.cows.update.selectedVaccination');

        return (selected == null || selected == 'no') ? 'disabled': '';
    },

    vaccinationsData: function() {
        return _.sortBy(this.vaccinations, 'date');
    }
});

Template.cowsUpdateVaccinations.events({
    'click .btn-add': function(e, t) {
        bootbox.dialog({
            title: 'Yeni aşı girdisi ekle',
            message: ' '
        });

        Blaze.renderWithData(Template.cowsUpdateVaccinationsAdd, null, $('.bootbox-body')[0]);
    },

    'click tr.item': function(e, t) {
        var $this = t.$(e.target);

        if (! $this.is('tr')) {
            $this = $this.closest('tr');
        }

        Session.set('farming.livestock.cows.update.selectedVaccination', this.registerNo);

        t.$('tr.item').removeClass('selected');
        $this.addClass('selected');
    },

    'click .btn-update:not(.disabled)': function(e, t) {
        bootbox.dialog({
            title: 'Aşı girdisini düzenle',
            message: ' '
        });

        Blaze.renderWithData(Template.cowsUpdateVaccinationsUpdate, null, $('.bootbox-body')[0]);
    },

    'click .btn-delete:not(.disabled)': function(e, t) {
        var id = Router.current().params._id,
            regNo = Session.get('farming.livestock.cows.update.selectedVaccination');

        Alert.confirm('Onayla!', 'Aşılama girdisini silmek istediğinizden emin misiniz?', function(res) {
            if (res) {
                Meteor.call('farming.livestock.cows.update.deleteVaccination', id, regNo, function() {
                    Session.set('farming.livestock.cows.update.selectedVaccination', 'no');

                    Notify.success('Aşılama girdisi başarıyla silindi.');
                });
            }
        });


    }
});

Template.cowsUpdateVaccinations.onRendered(function() {
    Session.set('farming.livestock.cows.update.selectedVaccination', null);

    this.autorun(function() {
        var selected = Session.get('farming.livestock.cows.update.selectedVaccination');

        if (selected == 'no'){
            $('tr.item').removeClass('selected');
        }
    });
});