Template.cowsUpdateStepAnalysis.helpers({

});

Template.cowsUpdateStepAnalysis.onRendered(function() {
    var $graphPanel = $('div.graph-panel'),
        graph = Morris.Area({
            element: 'stepAnalysisGraph',
            data: [],
            xkey: 'date',
            ykeys: ['count', 'oldCount'],
            labels: ['Bugün', 'Dün'],
            pointSize: 1,
            behaveLikeLine: true,
            gridTextColor: '#474e54',
            gridLineColor: '#eef0f2',
            goalLineColors: '#e3e6ea',
            gridTextFamily: $.configs.get('site', 'fontFamily'),
            gridTextWeight: '300',
            gridtextSize: 14,
            lineWidth: 1,
            fillOpacity: 0.1,
            lineColors: [$.colors("primary", 600), $.colors("green", 600)],
            dateFormat: function(x) {
                return moment(x).format('HH:mm');
            },
            yLabelFormat: function (y) {
                return parseInt(y);
            }
        });


    this.autorun(function() {
        var rfid = Template.currentData().rfid;

        $graphPanel.block();

        Meteor.subscribe('farming.livestock.cow.stepAnalysis', rfid, function() {
            var data = [],
                sc = StepCounts.find({
                    rfid: rfid,
                    date: {
                        $gte: moment().subtract(24, 'hours').toDate()
                    }
                }, {sort: {date: 1}}).fetch(),
                scy = StepCounts.find({
                    rfid: rfid,
                    date: {
                        $gte: moment().subtract(48, 'hours').toDate(),
                        $lt: moment().subtract(24, 'hours').toDate()
                    }
                }, {sort: {date: 1}}).fetch();

            for (var i = 0; i < sc.length; i++) {
                var d = sc[i],
                    dy = scy[i];

                data.push({
                    count: d.count,
                    oldCount: dy.count,
                    date: moment(d.date).format('YYYY-MM-DD HH:mm:ss')
                });
            }

            graph.setData(data);

            $graphPanel.unblock();

            data = null;
            sc = null;
        });
    });
});