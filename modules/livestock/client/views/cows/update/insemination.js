Template.cowsUpdateInsemination.helpers({
    isDisabled: function() {
        var selected = Session.get('farming.livestock.cows.update.selectedInsemination');

        return (selected == null || selected == 'no') ? 'disabled': '';
    },

    inseminationData: function() {
        return _.sortBy(this.insemination, 'date');
    },

    checkType: function(type) {
        return this.type == type;
    }
});

Template.cowsUpdateInsemination.events({
    'click .btn-add': function(e, t) {
        bootbox.dialog({
            title: 'Yeni tohumlama girdisi ekle',
            message: ' '
        });

        Blaze.renderWithData(Template.cowsUpdateInseminationAdd, null, $('.bootbox-body')[0]);
    },

    'click tr.item': function(e, t) {
        var $this = t.$(e.target);

        if (! $this.is('tr')) {
            $this = $this.closest('tr');
        }

        Session.set('farming.livestock.cows.update.selectedInsemination', this.registerNo);

        t.$('tr.item').removeClass('selected');
        $this.addClass('selected');
    },

    'click .btn-update:not(.disabled)': function(e, t) {
        bootbox.dialog({
            title: 'Tohumlama girdisini düzenle',
            message: ' '
        });

        Blaze.renderWithData(Template.cowsUpdateInseminationUpdate, null, $('.bootbox-body')[0]);
    },

    'click .btn-delete:not(.disabled)': function(e, t) {
        var id = Router.current().params._id,
            regNo = Session.get('farming.livestock.cows.update.selectedInsemination');

        Alert.confirm('Onayla!', 'Tohumlama girdisini silmek istediğinizden emin misiniz?', function(res) {
            if (res) {
                Meteor.call('farming.livestock.cows.update.deleteInsemination', id, regNo, function() {
                    Session.set('farming.livestock.cows.update.selectedInsemination', 'no');

                    Notify.success('Tohumlama girdisi başarıyla silindi.');
                });
            }
        });
    }
});

Template.cowsUpdateInsemination.onRendered(function() {
    Session.set('farming.livestock.cows.update.selectedInsemination', null);

    this.autorun(function() {
        var selected = Session.get('farming.livestock.cows.update.selectedInsemination');

        if (selected == 'no'){
            $('tr.item').removeClass('selected');
        }
    });
});