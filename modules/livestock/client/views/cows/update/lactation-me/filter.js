Template.cowsUpdateLactationMEFilters.events({
    'change #lactationMESelect': function(e) {
        var vals = $(e.target).val().split('|'),
            startDate = vals[0],
            endDate = vals[1];

        Session.set('farming.livestock.cows.lactationME.startDate', new Date(startDate));
        Session.set('farming.livestock.cows.lactationME.endDate', new Date(endDate));
    },

    'click .excel-btn': function() {
        var $graphPanel = $('div.graph-panel'),
            startDate = Session.get('farming.livestock.cows.lactationME.startDate'),
            endDate = Session.get('farming.livestock.cows.lactationME.endDate'),
            rfid = this.rfid,
            self = this;

        var base64ToBlob = function(base64String) {
            var byteArray, byteCharacters, byteNumbers, i;

            byteCharacters = atob(base64String);
            byteNumbers = new Array(byteCharacters.length);
            i = 0;

            while (i < byteCharacters.length) {
                byteNumbers[i] = byteCharacters.charCodeAt(i);
                i++;
            }

            byteArray = new Uint8Array(byteNumbers);

            return new Blob([byteArray], {type: 'application/vnd.openxmlformats'});
        };

        if (startDate && endDate && rfid) {
            $graphPanel.block();

            Meteor.call('farming.livestock.cows.exportMilkingData', rfid, startDate, endDate, function(error, data) {
                if (error) {
                    Alert.error('Hata Oluştu', error.reason);
                } else {
                    var blob = base64ToBlob(data);

                    saveAs(blob, 'sut-verimi.xlsx');

                    Logs.info(self.identity + ' kimliğine sahip ineğin laktasyonlara göre süt verimleri excele aktarıldı.');

                    $graphPanel.unblock();
                }
            });
        }
    }
});

Template.cowsUpdateLactationMEFilters.onRendered(function() {
    var $select = this.$('#lactationMESelect');

    Meteor.call('farming.livestock.cows.report', Router.current().params._id, function(error, report) {
        var lactationCount = report.lactionCount;

        if (lactationCount > 0) {
            for (var i = 0; i < lactationCount; i++) {
                var lStartDate = report.children[i].birthDate,
                    lEndDate = (report.children[i + 1]) ? report.children[i + 1].birthDate : moment().toDate();

                $select.append('<option value="' + lStartDate + '|' + lEndDate +'">' + (i + 1) + '. Laktasyon</option>');
            }

            $select.trigger('change');
        }
    });
});