Template.cowsUpdateLactationMEDetails.helpers({
    milkingData: function() {
        var startDate = Session.get('farming.livestock.cows.lactationME.startDate'),
            endDate = Session.get('farming.livestock.cows.lactationME.endDate'),
            rfid = this.rfid,
            md = null,
            data = {};

        data.days = 0;
        data.total = 0;
        data.dailyMilked = 0;
        data.monthlyMilked = 0;

        if (startDate && endDate) {
            md =  MilkData.find({
                rfid: rfid,
                date: {
                    $gte: startDate,
                    $lte: endDate
                }
            }, {sort: {date: 1}});

            data.days = md.count();
            data.total = 0;

            md.forEach(function(d) {
                data.total += d.amount;
            });

            data.dailyMilked = data.total / data.days;

            if (data.days >= 30) {
                var monthCount = data.days / 30;

                data.monthlyMilked = data.total / monthCount;
            } else {
                data.monthlyMilked = null;
            }

            data.days += ' Gün';
            data.total = data.total.toFixed(2) + ' Lt';
            data.dailyMilked = data.dailyMilked.toFixed(2) + ' Lt';
            data.monthlyMilked = data.monthlyMilked && data.monthlyMilked.toFixed(2) + ' Lt';
        }

        return data;
    }
});