AutoForm.hooks({
    createCowsForm: {
        before: {
            insert: function(doc) {
                var self = this;

                if (Cows.find({identity: doc.identity}).count() > 0) {
                    Cows.simpleSchema().namedContext('createCowsForm').addInvalidKeys([{name: 'identity', type: "notUnique"}]);
                    return false;
                }

                if (Cows.find({rfid: doc.rfid}).count() > 0) {
                    Cows.simpleSchema().namedContext('createCowsForm').addInvalidKeys([{name: 'rfid', type: "notUnique"}]);
                    return false;
                }

                var a = moment(),
                    b = moment(doc.birthDate),
                    c = a.diff(b, 'days');
                if (c < 0) {
                    Cows.simpleSchema().namedContext('createCowsForm').addInvalidKeys([{name: 'birthDate', type: "badDate"}]);
                }

                a = moment();
                b = moment(doc.joinDate);
                c = a.diff(b, 'days');
                if (c < 0) {
                    Cows.simpleSchema().namedContext('createCowsForm').addInvalidKeys([{name: 'joinDate', type: "badDate"}]);
                }

                Meteor.call('farming.livestock.cows.getRegisterNo', function(error, registerNo) {
                    doc.registerNo = registerNo;

                    Logs.info(doc.identity + ' kimliğine sahip inek oluşturuldu.');

                    self.result(doc);
                });
            }
        },
        onSuccess: function(formType, result) {
            Notify.success('Hayvan başarıyla oluşturuldu.');
            Router.go('farming.livestock.cows');
        }
    }
});

Template.cowsCreatePage.helpers({
    groups: function() {
        return Groups.find().fetch().map(function(group) {
            return {label: group.name, value: group._id};
        });
    }
});