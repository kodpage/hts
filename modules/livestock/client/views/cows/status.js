Template.cowStatus.helpers({
    checkStatus: function(status) {
        return _.contains(this.status, status);
    }
});