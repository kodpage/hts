Template.cowsUpdatePage.helpers({
    isActive: function(section) {
        var currentSection = Router.current().params.section || 'profile';

        return section == currentSection;
    },
    template: function() {
        var section = Router.current().params.section || 'profile';

        section = section.charAt(0).toUpperCase() + section.slice(1);

        return 'cowsUpdate' + section;
    }
});

Template.cowsUpdatePage.onRendered(function() {
    this.$('.page-header').append('<div class="page-header-actions"><h2>' + Template.currentData().identity + '</h2></div>');
});