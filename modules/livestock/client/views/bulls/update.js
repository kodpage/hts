AutoForm.hooks({
    updateBullsForm: {
        onSuccess: function(formType, result) {
            bootbox.hideAll();
            Notify.success('Boğa başarıyla güncellendi.');

            Logs.info('Boğa bilgileri güncellendi.');
        }
    }
});