Template.bullsPage.helpers({
    table: function() {
        return Tables.bulls
    },

    createAction: function() {
        return function() {
            bootbox.dialog({
                title: 'Yeni boğa ekle',
                message: ' '
            });

            Blaze.renderWithData(Template.bullsCreate, null, $('.bootbox-body')[0]);
        };
    },

    updateAction: function() {
        return function(id) {
            var data = Bulls.findOne({_id: id});

            bootbox.dialog({
                title: 'Boğayı düzenle',
                message: ' '
            });

            Blaze.renderWithData(Template.bullsUpdate, data, $('.bootbox-body')[0]);
        };
    },

    deleteAction: function() {
        return {
            msg: 'Seçilen boğaları silmek istediğinize emin misiniz?',
            callback: function(isConfirmed) {
                if (isConfirmed) {
                    var bulls = Session.get('farming.livestock.bulls.selectedRows');

                    for (var i = 0; i < bulls.length; i++) {
                        Bulls.remove({_id: bulls[i]._id});
                    }
                }
            }
        };
    }
});