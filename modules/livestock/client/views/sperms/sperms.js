Template.spermsPage.helpers({
    table: function() {
        return Tables.sperms
    },

    createAction: function() {
        return function() {
            bootbox.dialog({
                title: 'Yeni sperm ekle',
                message: ' '
            });

            Blaze.renderWithData(Template.spermsCreate, null, $('.bootbox-body')[0]);
        };
    },

    updateAction: function() {
        return function(id) {
            var data = Sperms.findOne({_id: id});

            bootbox.dialog({
                title: 'Spermi düzenle',
                message: ' '
            });

            Blaze.renderWithData(Template.spermsUpdate, data, $('.bootbox-body')[0]);
        };
    },

    deleteAction: function() {
        return {
            msg: 'Seçilen spermleri silmek istediğinize emin misiniz?',
            callback: function(isConfirmed) {
                if (isConfirmed) {
                    var sperms = Session.get('farming.livestock.sperms.selectedRows');

                    for (var i = 0; i < sperms.length; i++) {
                        Sperms.remove({_id: sperms[i]._id});
                    }
                }
            }
        };
    }
});