CowsCreateController = PanelController.extend({
    template: 'cowsCreatePage',

    waitOn: function() {
        return [
            Subs.subscribe('farming.livestock.groups')
        ];
    },

    action: function() {
        this.render();
    }
});