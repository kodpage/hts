CowsUpdateController = PanelController.extend({
    template: 'cowsUpdatePage',

    waitOn: function() {
        return [
            Subs.subscribe('farming.livestock.groups'),
            Subs.subscribe('farming.livestock.cow', this.params._id),
            Subs.subscribe('farming.livestock.bulls'),
            Subs.subscribe('farming.livestock.sperms')
        ];
    },

    data: function () {
        return Cows.findOne({_id: this.params._id});
    },

    action: function() {
        this.render();
    }
});