CowsController = PanelController.extend({
    template: 'cowsPage',

    waitOn: function() {
        return [
            Subs.subscribe('farming.livestock.groups')
        ];
    },

    action: function() {
        this.render();
    }

});