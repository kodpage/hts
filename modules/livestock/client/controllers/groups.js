GroupsController = PanelController.extend({
    template: 'groupsPage',

    waitOn: function() {
        return [
            Subs.subscribe('farming.livestock.groups')
        ];
    },

    action: function() {
        this.render();
    }

});