Meteor.publish('farming.reports.milking.timed', function(startDate, endDate) {
    return MilkDataSums.find({
        date: {
            $gte: startDate,
            $lte: endDate
        }
    }, {sort: {date: 1}});
});

Meteor.publish('farming.reports.heats', function() {
    var self = this,
        initializing = true,
        handle = null,
        normalHourlyStepCount = Settings.get('farming.normalHourlyStepCount'),
        abnormalHourlyStepCount = Settings.get('farming.abnormalHourlyStepCount'),
        cows = [];

    handle = StepCounts.find({
        count: {
            $gte: abnormalHourlyStepCount
        },
        date: {
            $gte: moment().subtract(1, 'hours').minute(0).toDate()
        }
    }, {sort: {date: -1, count: -1}}).observeChanges({
        added: function(id, fields) {
            if (!_.findWhere(cows, {identity: fields.identity})) {
                var cow = Cows.findOne({identity: fields.identity}),
                    yesterdayData = StepCounts.findOne({
                        identity: fields.identity,
                        date: {
                            $gte: moment(fields.date).date(moment(fields.date).date() - 1).minute(0).toDate(),
                            $lte: moment(fields.date).date(moment(fields.date).date() - 1).minute(59).toDate()
                        }
                    }),
                    todayMilkData = MilkData.findOne({
                        rfid: fields.rfid,
                        date: {
                            $gte: moment(fields.date).hour(0).minute(0).toDate(),
                            $lte: moment(fields.date).hour(23).minute(59).toDate()
                        }
                    }),
                    yesterdayMilkData = MilkData.findOne({
                        rfid: fields.rfid,
                        date: {
                            $gte: moment(fields.date).date(moment(fields.date).date() - 1).hour(0).minute(0).toDate(),
                            $lte: moment(fields.date).date(moment(fields.date).date() - 1).hour(23).minute(59).toDate()
                        }
                    });

                cows.push({
                    identity: fields.identity,
                    rfid: fields.rfid,
                    cowId: cow._id,
                    todayCount: fields.count,
                    yesterdayCount: (_.isObject(yesterdayData)) ? yesterdayData.count : null,
                    todayMilkAmount:  (_.isObject(todayMilkData)) ? todayMilkData.amount : null,
                    yesterdayMilkAmount: (_.isObject(yesterdayMilkData)) ? yesterdayMilkData.amount : null
                });
            }

            if (!initializing) {
                self.changed('cows-at-heat', 1, {cows: cows});
            }
        }
    });


    initializing = false;
    self.added('cows-at-heat', 1, {cows: cows});
    self.ready();

    self.onStop(function() {
        handle.stop();
    });
});