Permission.add({
    name: 'farming.reports.milking',
    label: 'Süt verimi raporları erişimi',
    group: 'Raporlar'
});
Permission.add({
    name: 'farming.reports.heats',
    label: 'Kızgınlık raporları erişimi',
    group: 'Raporlar'
});