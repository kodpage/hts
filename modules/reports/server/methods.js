Meteor.methods({
    'farming.reports.milking.timed.exportMilkingData': function(startDate, endDate) {
        var excel = new Excel('xlsx'); // Create an excel object  for the file you want (xlsx or xls)
        var workbook = excel.createWorkbook(); // Create a workbook (equivalent of an excel file)
        var worksheet = excel.createWorksheet(); // Create a worksheet to be added to the workbook

        worksheet.writeToCell(0, 0, 'Tarih');
        worksheet.writeToCell(0, 1, 'Sağılan');
        worksheet.writeToCell(0, 2, 'Miktar (Lt)');
        worksheet.writeToCell(0, 3, 'Ortalama (Lt)');
        worksheet.writeToCell(0, 4, 'Toplam (Lt)');

        var days = 0,
            total = 0,
            row = 1;

        MilkDataSums.find({
            date: {
                $gte: startDate,
                $lte: endDate
            }
        }, {sort: {date: 1}}).forEach(function(d) {
            days++;
            total += d.amount;

            worksheet.writeToCell(row, 0, moment(d.date).format('DD.MM.YYYY'));
            worksheet.writeToCell(row, 1, d.rfids.length);
            worksheet.writeToCell(row, 2, d.amount.toFixed(2));
            worksheet.writeToCell(row, 3, (total / days).toFixed(2));
            worksheet.writeToCell(row, 4, total.toFixed(2));

            row++;
        });

        workbook.addSheet('Süt Verimi', worksheet);

        var workBookOut = workbook.write({bookType: 'xlsx', bookSST: false, type: 'binary'});

        return new Buffer(workBookOut, 'binary').toString('base64');
    },

    'farming.reports.milking.monthly.exportMilkingData': function(startDate, endDate) {
        var excel = new Excel('xlsx'); // Create an excel object  for the file you want (xlsx or xls)
        var workbook = excel.createWorkbook(); // Create a workbook (equivalent of an excel file)
        var worksheet = excel.createWorksheet(); // Create a worksheet to be added to the workbook

        worksheet.writeToCell(0, 0, 'Tarih');
        worksheet.writeToCell(0, 1, 'Miktar (Lt)');
        worksheet.writeToCell(0, 2, 'Ortalama (Lt)');

        var records = {},
            total = 0,
            row = 1,
            data = [];

        MilkDataSums.find({
            date: {
                $gte: startDate,
                $lte: endDate
            }
        }, {sort: {date: 1}}).forEach(function(d) {
            var record = records[moment(d.date).format('MM.YYYY')];

            total += d.amount;

            if (_.isObject(record)) {
                record.amount += d.amount;
                record.average = 0;
            } else {
                record = {
                    date: moment(d.date).format('MM.YYYY'),
                    amount: d.amount,
                    average: 0
                };
            }

            records[moment(d.date).format('MM.YYYY')] = record;
        });

        for (var date in records) {
            if (records.hasOwnProperty(date)) {
                var record = records[date];

                data.push({
                    amount: record.amount,
                    date: date,
                    average: (total / Object.keys(records).length)
                })
            }
        }

        if (data.length > 0) {
            _.each(data, function(d) {
                worksheet.writeToCell(row, 0, d.date);
                worksheet.writeToCell(row, 1, d.amount.toFixed(2));
                worksheet.writeToCell(row, 2, d.average.toFixed(2));

                row++;
            });
        }

        workbook.addSheet('Süt Verimi', worksheet);

        var workBookOut = workbook.write({bookType: 'xlsx', bookSST: false, type: 'binary'});

        return new Buffer(workBookOut, 'binary').toString('base64');
    },

    'farming.reports.findYears': function() {
        var sums = MilkDataSums.find({}, {sort: {date: 1}}).fetch(),
            years = [];

        if (sums.length > 0) {
            var firstYear = moment(sums[0].date).year(),
                lastYear = moment(sums[sums.length -1].date).year();

            for (var year = firstYear; year <= lastYear; year++) {
                years.push(year);
            }
        }

        return years;
    }
});