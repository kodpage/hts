ReportsHeatsController = PanelController.extend({
    template: 'reportsHeatsPage',

    waitOn: function() {
        return [
            Subs.subscribe('farming.reports.heats')
        ];
    },

    action: function() {
        this.render();
    }

});