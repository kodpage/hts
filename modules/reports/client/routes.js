Router.route('/farming/reports/milking/:section?', {
    name: 'farming.reports.milking',
    title: 'Süt Verimi',
    controller: ReportsMilkingController
});

Router.route('/farming/reports/heats', {
    name: 'farming.reports.heats',
    title: 'Kızgınlık',
    controller: ReportsHeatsController
});