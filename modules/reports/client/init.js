Menu.set('farming.reports', {
    title: 'Raporlar',
    name: 'farming.reports',
    icon: 'wb-stats-bars',
    order: 2
});

Menu.set('farming.reports.milking', {
    title: 'Süt Verimi',
    name: 'farming.reports.milking',
    order: 0
});

Menu.set('farming.reports.heats', {
    title: 'Kızgınlık',
    name: 'farming.reports.heats',
    order: 1
});