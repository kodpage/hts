Template.reportsMilkingPage.helpers({
    isActive: function(section) {
        var currentSection = Router.current().params.section || 'timed';

        return section == currentSection;
    },
    template: function() {
        var section = Router.current().params.section || 'timed';

        section = section.charAt(0).toUpperCase() + section.slice(1);

        return 'reportsMilking' + section;
    }
});