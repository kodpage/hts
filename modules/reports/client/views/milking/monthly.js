Template.reportsMilkingMonthly.events({
    'change #yearSelect': function(e) {
        var val = $(e.target).val();

        Session.set('farming.reports.cows.monthly.year', val);
    },

    'click .excel-btn': function() {
        var $graphPanel = $('div.graph-panel'),
            year = Session.get('farming.reports.cows.monthly.year'),
            currentYear = moment().year(),
            startDate = null,
            endDate = null;

        var base64ToBlob = function(base64String) {
            var byteArray, byteCharacters, byteNumbers, i;

            byteCharacters = atob(base64String);
            byteNumbers = new Array(byteCharacters.length);
            i = 0;

            while (i < byteCharacters.length) {
                byteNumbers[i] = byteCharacters.charCodeAt(i);
                i++;
            }

            byteArray = new Uint8Array(byteNumbers);

            return new Blob([byteArray], {type: 'application/vnd.openxmlformats'});
        };

        if (year) {
            startDate = moment(year + '-01-01 00:00:00');

            if (year == currentYear) {
                endDate = moment()
            } else {
                endDate = moment(year + '-12-31 23:59:59')
            }
        }

        if (startDate && endDate) {
            $graphPanel.block();

            Meteor.call('farming.reports.milking.monthly.exportMilkingData', startDate.toDate(), endDate.toDate(), function(error, data) {
                if (error) {
                    Alert.error('Hata Oluştu', error.reason);
                } else {
                    var blob = base64ToBlob(data);

                    saveAs(blob, 'aylik-sut-verimleri.xlsx');

                    Logs.info('Aylık süt verim raporları excele aktarıldı.');

                    $graphPanel.unblock();
                }
            });
        }
    }
});

Template.reportsMilkingMonthly.onRendered(function() {
    var $select = this.$('#yearSelect');

    Meteor.call('farming.reports.findYears', function(error, years) {
        if (years && years.length > 0) {
            years = years.reverse();
            _.each(years, function(year) {
                $select.append('<option value="' + year + '">' + year + ' süt verimeleri</option>');
            });

            $select.trigger('change');
        }
    });

    var $graphPanel = $('div.graph-panel'),
        graph = Morris.Bar({
            element: 'milkingMonthlyGraph',
            data: [],
            xkey: 'date',
            ykeys: ['amount', 'average'],
            labels: ['Miktar', 'Ortalama'],
            smooth: true,
            barGap: 6,
            barSizeRatio: 0.35,
            gridTextColor: '#474e54',
            gridLineColor: '#eef0f2',
            goalLineColors: '#e3e6ea',
            gridTextFamily: $.configs.get('site', 'fontFamily'),
            gridTextWeight: '300',
            gridtextSize: 14,
            barColors: [$.colors("light-green", 600), $.colors("blue-grey", 300)],
            //dateFormat: function(x) {
            //    return moment(x).format('DD.MM.YYYY');
            //},
            yLabelFormat: function(y) {
                return y.toFixed(2) + ' Lt';
            }
        });

    this.autorun(function() {
        var year = Session.get('farming.reports.cows.monthly.year'),
            currentYear = moment().year(),
            startDate = null,
            endDate = null;

        if (year) {
            startDate = moment(year + '-01-01 00:00:00');

            if (year == currentYear) {
                endDate = moment()
            } else {
                endDate = moment(year + '-12-31 23:59:59')
            }

            $graphPanel.block();

            Meteor.subscribe('farming.reports.milking.timed', startDate.toDate(), endDate.toDate(), function() {
                var records = {},
                    total = 0,
                    data = [];

                MilkDataSums.find({
                    date: {
                        $gte: startDate.toDate(),
                        $lte: endDate.toDate()
                    }
                }, {sort: {date: 1}}).forEach(function(d) {
                    var record = records[moment(d.date).format('YYYY-MM')];

                    total += d.amount;

                    if (_.isObject(record)) {
                        record.amount += d.amount;
                        record.average = 0;
                    } else {
                        record = {
                            date: moment(d.date).format('YYYY-MM'),
                            amount: d.amount,
                            average: 0
                        };
                    }

                    records[moment(d.date).format('YYYY-MM')] = record;
                });

                for (var date in records) {
                    if (records.hasOwnProperty(date)) {
                        var record = records[date];

                        data.push({
                            amount: record.amount,
                            date: date,
                            average: (total / Object.keys(records).length)
                        })
                    }
                }

                graph.setData(data);

                $graphPanel.unblock();
            });
        }
    });
});