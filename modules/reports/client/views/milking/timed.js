Template.reportsMilkingTimed.helpers({
    milkingData: function() {
        var startDate = Session.get('farming.reports.milking.timed.startDate'),
            endDate = Session.get('farming.reports.milking.timed.endDate'),
            md = null,
            data = {};

        data.days = 0;
        data.total = 0;
        data.dailyMilked = 0;
        data.monthlyMilked = 0;
        data.yearlyMilked = 0;

        if (startDate && endDate) {
            md =  MilkDataSums.find({
                date: {
                    $gte: startDate,
                    $lte: endDate
                }
            }, {sort: {date: 1}});

            data.days = md.count();
            data.total = 0;

            md.forEach(function(d) {
                data.total += d.amount;
            });

            data.dailyMilked = data.total / data.days;

            if (data.days >= 30) {
                var monthCount = data.days / 30;

                data.monthlyMilked = data.total / Math.round(monthCount);
            } else {
                data.monthlyMilked = null;
            }

            if (data.days >= 365) {
                var yearCount = Math.round(data.days / 365);

                data.yearlyMilked = data.total / yearCount;
            } else {
                data.yearlyMilked = null;
            }

            data.days += ' Gün';
            data.total = data.total.toFixed(2) + ' Lt';
            data.dailyMilked = data.dailyMilked.toFixed(2) + ' Lt';
            data.monthlyMilked = data.monthlyMilked && data.monthlyMilked.toFixed(2) + ' Lt';
            data.yearlyMilked = data.yearlyMilked && data.yearlyMilked.toFixed(2) + ' Lt';
        }

        return data;
    }
});

Template.reportsMilkingTimed.events({
    'click .excel-btn': function() {
        var $graphPanel = $('div.graph-panel'),
            startDate = Session.get('farming.reports.milking.timed.startDate'),
            endDate = Session.get('farming.reports.milking.timed.endDate');

        var base64ToBlob = function(base64String) {
            var byteArray, byteCharacters, byteNumbers, i;

            byteCharacters = atob(base64String);
            byteNumbers = new Array(byteCharacters.length);
            i = 0;

            while (i < byteCharacters.length) {
                byteNumbers[i] = byteCharacters.charCodeAt(i);
                i++;
            }

            byteArray = new Uint8Array(byteNumbers);

            return new Blob([byteArray], {type: 'application/vnd.openxmlformats'});
        };

        if (startDate && endDate) {
            $graphPanel.block();

            Meteor.call('farming.reports.milking.timed.exportMilkingData', startDate, endDate, function(error, data) {
                if (error) {
                    Alert.error('Hata Oluştu', error.reason);
                } else {
                    var blob = base64ToBlob(data);

                    saveAs(blob, 'sut-verimi.xlsx');

                    Logs.info('Zamansal süt verim raporu excele aktarıldı.');

                    $graphPanel.unblock();
                }
            });
        }
    }
});

Template.reportsMilkingTimed.onRendered(function() {

    /*
     * FILTER
     */
    var $dateRange = this.$('.input-daterange');

    $dateRange.datepicker({
        endDate: '+1d',
        language: 'tr',
        todayHighlight: true
    });

    $dateRange.find('input[name="start"]').datepicker('setDate', moment().subtract(180, 'days').format('DD.MM.YYYY'));
    $dateRange.find('input[name="end"]').datepicker('setDate', moment().format('DD.MM.YYYY'));

    $dateRange.datepicker().on('changeDate', function(e) {
        if ($(e.target).is('input[name="end"]')) {
            Session.set('farming.reports.milking.timed.endDate', moment(e.date).toDate());
        } else {
            Session.set('farming.reports.milking.timed.startDate', moment(e.date).toDate());
        }
    });

    Session.set('farming.reports.milking.timed.startDate', moment().subtract(180, 'days').toDate());
    Session.set('farming.reports.milking.timed.endDate', moment().toDate());

    $dateRange.find('input[name="start"], input[name="end"]').datepicker().on('show', function() {
        $('.datepicker.datepicker-dropdown').css('z-index', 1400);
    });

    /*
     * GRAPH
     */
    var $graphPanel = $('div.graph-panel'),
        graph = Morris.Line({
            element: 'milkingTimedGraph',
            data: [],
            xkey: 'date',
            ykeys: ['amount'],
            labels: ['Miktar'],
            //resize: true,
            pointSize: 1,
            smooth: true,
            gridTextColor: '#474e54',
            gridLineColor: '#eef0f2',
            goalLineColors: '#e3e6ea',
            gridTextFamily: $.configs.get('site', 'fontFamily'),
            gridTextWeight: '300',
            //numLines: 9,
            gridtextSize: 14,
            lineWidth: 1,
            lineColors: [$.colors('primary', 700)],
            postUnits: ' Lt',
            dateFormat: function(x) {
                return moment(x).format('DD.MM.YYYY');
            },
            yLabelFormat: function (y) {
                return y.toFixed(2) + ' Lt';
            }
        });

    this.autorun(function() {
        var startDate = Session.get('farming.reports.milking.timed.startDate'),
            endDate = Session.get('farming.reports.milking.timed.endDate');

        if (startDate || endDate) {
            $graphPanel.block();

            Meteor.subscribe('farming.reports.milking.timed', startDate, endDate, function() {
                var data = MilkDataSums.find({
                    date: {
                        $gte: startDate,
                        $lte: endDate
                    }
                }, {sort: {date: 1}}).fetch().map(function(d) {
                    return {
                        amount: d.amount,
                        date: moment(d.date).format('YYYY-MM-DD')
                    };
                });

                graph.setData(data);

                $graphPanel.unblock();
            });
        }
    });
});