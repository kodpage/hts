Heats = new Meteor.Collection('cows-at-heat');

var maxPercentage = 0;

Template.reportsHeatsPage.helpers({
    cows: function() {
        var cows = [];

        Heats.findOne({_id: 1}).cows.forEach(function(data) {
            data = _.extend(data, {
                formattedAmount: (function() {
                    if (data.todayMilkAmount) {
                        return data.todayMilkAmount.toFixed(2) + ' Lt';
                    } else {
                        return '-'
                    }
                })(),
                formattedYesterdayAmount: (function() {
                    if (data.yesterdayMilkAmount) {
                        return data.yesterdayMilkAmount.toFixed(2) + ' Lt';
                    } else {
                        return '-'
                    }
                })(),
                calculatedCount: (function() {
                    var count = data.todayCount,
                        previousCount = data.yesterdayCount;

                    if (count && previousCount) {
                        return (((count - previousCount) / previousCount) * 100).toFixed(2) + '%';
                    } else {
                        return '-';
                    }
                })(),
                calculatedAmount: (function() {
                    var amount = data.todayMilkAmount,
                        previousAmount = data.yesterdayMilkAmount;

                    if (amount && previousAmount) {
                        return (((previousAmount - amount) / previousAmount) * 100).toFixed(2) + '%';
                    } else {
                        return '-';
                    }
                })(),
                headPercentage: (function() {
                    var count = data.todayCount,
                        previousCount = data.yesterdayCount,
                        amount = data.todayMilkAmount,
                        previousAmount = data.yesterdayMilkAmount;

                    if (count && previousCount) {
                        var stepPercentage = ((count - previousCount) / previousCount) * 100;

                        if (amount && previousAmount) {
                            var milkPercentage = ((previousAmount - amount) / previousAmount) * 100;

                            return (stepPercentage * 0.4 + milkPercentage * 0.6);
                        } else {
                            return stepPercentage;
                        }
                    } else {
                        return 0;
                    }
                })()
            });

            if (data.headPercentage > maxPercentage) {
                maxPercentage = data.headPercentage;
            }

            cows.push(data);
        });

        return _.sortBy(cows, 'headPercentage').reverse();
    },

    estimation: function() {
        var range = parseInt(maxPercentage / 3);

        if (this.headPercentage <= range) {
            return '<span class="label label-round label-dark">Düşük</span>';
        } else if (this.headPercentage > range && this.headPercentage <= (range * 2)) {
            return '<span class="label label-round label-warning">Orta</span>';
        } else {
            return '<span class="label label-round label-danger">Yüksek</span>';
        }
    }
});