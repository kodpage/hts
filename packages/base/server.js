// Inject meta tags
Inject.rawHead('charset-meta', '<meta charset="utf-8">');
Inject.rawHead('title-meta', '<title>HTS</title>');
Inject.rawHead('author-meta', '<meta name="author" content="Mahmut AKTAS <maktass@icloud.com">');
Inject.rawHead('http-equiv', '<meta http-equiv="X-UA-Compatible" content="IE=edge">');
Inject.rawHead('viewport-meta', '<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">');

// Inject icons
Inject.rawHead('apple-touch-icon', '<link rel="apple-touch-icon" href="/assets/images/apple-touch-icon.png">');
Inject.rawHead('shortcut icon', '<link rel="shortcut icon" href="/assets/images/favicon.ico">');

// Inject fonts
Inject.rawHead('web-icons', '<link rel="stylesheet" href="/assets/fonts/web-icons/web-icons.min.css">');
Inject.rawHead('weather-icons', '<link rel="stylesheet" href="/assets/fonts/weather-icons/weather-icons.min.css">');
Inject.rawHead('brand-icons', '<link rel="stylesheet" href="/assets/fonts/brand-icons/brand-icons.min.css">');
Inject.rawHead('web-font', '<link rel="stylesheet" href="/assets/css/font.css">');

Meteor.startup(function () {
    console.log('Kodapp started.');
});