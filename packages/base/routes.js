// Main route
Router.route('/', function() {
    if (Meteor.userId()) {
        Router.go('farming.dashboard');
    } else {
        Router.go('login');
    }
});

// Login route
Router.route('/login', {
    name: 'login',
    title: 'Giriş Yap',
    controller: LoginController
});
Router.route('/locked', {
    name: 'locked',
    title: 'Hesaba Geri Dön',
    controller: LockedController
});