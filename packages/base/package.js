Package.describe({
    name: 'hts:base',
    version: '0.0.1'
});

Package.onUse(function (api) {
    api.versionsFrom('1.2.1');


    // ----------------------------------------------
    // DEPENDENCIES
    // ----------------------------------------------
    api.use([
        'ecmascript',
        'underscore',
        'ejson',
        'mongo',
        'check',
        'less',
        'momentjs:moment',
        'aldeed:moment-timezone',
        'iron:router',
        'meteorhacks:inject-initial',
        'aldeed:autoform',
        'aldeed:template-extension',
        'hts:libraries'
    ]);
    api.use([
        'jquery',
        'blaze',
        'templating',
        'tracker',
        'reactive-var'
    ], 'client');
    api.use([
    ], 'server');

    api.imply([
        'hts:libraries',
        'momentjs:moment'
    ]);
    // ----------------------------------------------


    // ----------------------------------------------
    // ASSETS
    // ----------------------------------------------
    // Static Assets
    api.addAssets([
        // Images
    ], 'client');

    api.addFiles([
        // Vendor Styles
        'assets/vendor/bootstrap//bootstrap.css',
        'assets/vendor/asscrollable/asScrollable.css',
        'assets/vendor/switchery/switchery.css',
        'assets/vendor/intro-js/introjs.css',
        'assets/vendor/slidepanel/slidePanel.css',
        'assets/vendor/flag-icon-css/flag-icon.css',
        'assets/vendor/toastr/toastr.css',
        'assets/vendor/datatables-bootstrap/dataTables.bootstrap.css',
        'assets/vendor/datatables-fixedheader/dataTables.fixedHeader.css',
        'assets/vendor/datatables-responsive/dataTables.responsive.css',
        'assets/vendor/bootstrap-sweetalert/sweet-alert.css',
        'assets/vendor/bootstrap-datepicker/bootstrap-datepicker.css',
        'assets/vendor/select2/select2.css',
        'assets/vendor/morris-js/morris.css',
        'assets/vendor/chartist-js/chartist.css',
        'assets/css/chart.css',

        // Custom Styles
        'assets/css/bootstrap-extend.css',
        'assets/css/site.css',

        // Custom Less
        'assets/less/base/main.import.less',
        'assets/less/styles.less',

        // Vendor Scripts
        'assets/vendor/bootstrap/bootstrap.min.js',
        'assets/vendor/asscroll/jquery-asScroll.min.js',
        'assets/vendor/mousewheel/jquery.mousewheel.min.js',
        'assets/vendor/asscrollable/jquery.asScrollable.all.min.js',
        'assets/vendor/ashoverscroll/jquery-asHoverScroll.min.js',
        'assets/vendor/switchery/switchery.min.js',
        'assets/vendor/intro-js/intro.min.js',
        'assets/vendor/screenfull/screenfull.js',
        'assets/vendor/slidepanel/jquery-slidePanel.min.js',
        'assets/vendor/toastr/toastr.js',
        'assets/vendor/datatables/jquery.dataTables.js',
        'assets/vendor/datatables-fixedheader/dataTables.fixedHeader.js',
        'assets/vendor/datatables-bootstrap/dataTables.bootstrap.js',
        'assets/vendor/datatables-responsive/dataTables.responsive.js',
        'assets/vendor/datatables-tabletools/dataTables.tableTools.js',
        'assets/vendor/bootstrap-sweetalert/sweet-alert.js',
        'assets/vendor/breakpoints/breakpoints.js',
        'assets/vendor/modernizr/modernizr.js',
        'assets/vendor/bootstrap-datepicker/bootstrap-datepicker.js',
        'assets/vendor/bootbox/bootbox.js',
        'assets/vendor/select2/select2.js',
        'assets/vendor/raphael/raphael.js',
        'assets/vendor/morris-js/morris.js',
        'assets/vendor/jquery.blockUI.js',
        'assets/vendor/matchheight/jquery.matchHeight.js',
        'assets/vendor/chartist-js/chartist.js',

        // Core Scripts
        'assets/js/core.js',
        'assets/js/site.js',
        'assets/js/sections/menu.js',
        'assets/js/sections/menubar.js',
        'assets/js/sections/sidebar.js',

        // Config Scripts
        'assets/js/configs/config-colors.js',
        'assets/js/configs/config-tour.js',

        // Component Scripts
        'assets/js/components/asscrollable.js',
        'assets/js/components/switchery.js',
        'assets/js/components/slidepanel.js'
    ], 'client');
    // ----------------------------------------------


    // ----------------------------------------------
    // COMPONENTS
    // ----------------------------------------------
    // Helpers
    api.addFiles([
        'components/helpers/mixed.js'
    ], 'client');

    // Menu
    api.addFiles([
        'components/menu/menu.js'
    ], 'client');

    // Content
    api.addFiles([
        'components/content/content.html',
        'components/content/content.js'
    ], 'client');

    // Form
    api.addFiles([
        'components/form/form.html',
        'components/form/form.js'
    ], 'client');

    // Field
    api.addFiles([
        'components/field/overrides/bootstrap-checkbox.html',
        'components/field/select2/select2.html',
        'components/field/select2/select2.js',
        'components/field/field.html',
        'components/field/field.js'
    ], 'client');

    // Notify
    api.addFiles([
        'components/notify/notify.js'
    ], 'client');

    // Alert
    api.addFiles([
        'components/alert/alert.js'
    ], 'client');

    // Tabular
    api.addFiles([
        'components/tabular/client/checkbox-cell.html',
        'components/tabular/client/checkbox-cell.js'
    ], 'client');
    api.addFiles([
        'components/tabular/common.js',
        'components/tabular/tables.js'
    ]);
    api.addFiles([
        'components/tabular/server/tabular.js'
    ], 'server');
    api.addFiles([
        'components/tabular/client/tabular.html',
        'components/tabular/client/util.js',
        'components/tabular/client/table-records.js',
        'components/tabular/client/table-init.js',
        'components/tabular/client/pub-selector.js',
        'components/tabular/client/tabular.js'
    ], 'client');
    // ----------------------------------------------


    // ----------------------------------------------
    // PACKAGE
    // ----------------------------------------------
    // Views
    api.addFiles([
        // Common
        'views/common/head.html',
        'views/common/body.html',
        'views/common/loading.html',

        // Panel
        'views/panel/layout/panel.html',
        'views/panel/layout/panel.js',
        'views/panel/partials/navbar/header.html',
        'views/panel/partials/navbar/toolbar.html',
        'views/panel/partials/navbar/search.html',
        'views/panel/partials/navbar/notifications.html',
        'views/panel/partials/navbar/notifications.js',
        'views/panel/partials/navbar/user.html',
        'views/panel/partials/navbar/user.js',
        'views/panel/partials/navbar.html',
        'views/panel/partials/menubar/menu.html',
        'views/panel/partials/menubar/menu.js',
        'views/panel/partials/menubar/footer.html',
        'views/panel/partials/menubar/footer.js',
        'views/panel/partials/menubar.html',
        'views/panel/partials/footer.html',

        // Login
        'views/login/layout/login.html',
        'views/login/layout/login.js',
        'views/login/pages/login.html',
        'views/login/pages/login.js',
        'views/login/pages/locked.html',
        'views/login/pages/locked.js'
    ], 'client');

    // Controllers
    api.addFiles([
        'controllers/base.js',
        'controllers/panel.js',
        'controllers/login.js',
        'controllers/locked.js'
    ], 'client');

    // Package files
    api.addFiles('client.js', 'client');
    api.addFiles('server.js', 'server');

    // Routes
    api.addFiles('routes.js', 'client');
    // ----------------------------------------------


    // ----------------------------------------------
    // EXPORTS
    // ----------------------------------------------
    // Components
    api.export([
        'Menu',
        'Notify',
        'Alert'
    ], 'client');
    api.export([
        'Tabular',
        'Tables'
    ]);

    // Controllers
    api.export([
        'BaseController',
        'PanelController',
    ], 'client');
    // ----------------------------------------------
});
