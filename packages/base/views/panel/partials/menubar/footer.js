Template.panelMenubarFooter.helpers({
    userId: function() {
        return Meteor.userId();
    }
});

Template.panelMenubarFooter.events({
    'click .logout': function() {
        var $body = $('body');

        $body.block();

        Meteor.logout(function() {
            $body.unblock();
            Router.go('login');
        });
    },

    'click .lock-screen': function() {
        var username = Meteor.user().username,
            $body = $('body');

        localStorage.setItem('locked-user', username);

        $body.block();

        Meteor.logout(function() {
            $body.unblock();

            Router.go('locked');
        });
    }
});