Template.panelMenubarMenu.helpers({
    sections: function() {
        return Menu.sections();
    },

    items: function() {
        return Menu.get(this.name);
    },

    subItems: function() {
        return Menu.get(this.name);
    }
});

Template.panelMenubarMenu.events({
});

Template.panelMenubarMenu.onRendered(function() {
    $.site.menu.init();
});