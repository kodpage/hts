Template.panelNavbarUser.helpers({
    userId: function() {
        return Meteor.userId();
    }
});

Template.panelNavbarUser.events({
    'click #userLockScreen': function() {
        var username = Meteor.user().username,
            $body = $('body');

        localStorage.setItem('locked-user', username);

        $body.block();

        Meteor.logout(function() {
            $body.unblock();

            Router.go('locked');
        });
    },
    'click #userLogOut': function() {
        var $body = $('body');

        $body.block();

        Meteor.logout(function() {
            $body.unblock();
            Router.go('login');
        });
    }
});