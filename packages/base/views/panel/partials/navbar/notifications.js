NewNotificationsCount = new Mongo.Collection('new-notifications-count');

Template.panelNavbarNotifications.helpers({
    notifications: function() {
        return Notifications.find({
            isRead: false,
            date: {
                $gte: moment().subtract(1, 'days').toDate()
            }
        }, {sort: {date: -1}, limit: 4}).fetch();
    },

    newNotificationCount: function() {
        var nc = NewNotificationsCount.findOne(1);

        if (nc) {
            return nc.count;
        }
    },

    typeIcon: function() {
        var icon = '';

        switch (this.type) {
            case 'system':
                icon = 'wb-grid-9 bg-green-600';
                break;
            case 'warning':
                icon = 'wb-alert bg-orange-600';
                break;
            case 'error':
                icon = 'wb-close bg-red-600';
                break;
            case 'info':
                icon = 'wb-info bg-blue-600';
                break;
        }

        return icon;
    },

    typeColor: function() {
        var color = '';

        switch (this.type) {
            case 'system':
                color = 'green-600';
                break;
            case 'warning':
                color = 'orange-600';
                break;
            case 'error':
                color = 'red-600';
                break;
            case 'info':
                color = 'blue-600';
                break;
        }

        return color;
    },

    typeLabel: function() {
        var label = '';

        switch (this.type) {
            case 'system':
                label = 'Sisttem';
                break;
            case 'warning':
                label = 'Uyarı';
                break;
            case 'error':
                label = 'Hata';
                break;
            case 'info':
                label = 'Bilgi';
                break;
        }

        return label;
    },

    timeAgo: function() {
        return moment(this.date).fromNow();
    }
});