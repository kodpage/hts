AutoForm.hooks({
    lockedForm: {
        onSubmit: function(fields) {
            var that = this,
                username = localStorage.getItem('locked-user');

            Meteor.loginWithPassword(username, fields.password, function(error) {
                if (error) {
                    Notify.error('Şifre hatalı!');
                    that.done();
                } else {
                    Logs.info(username + ' kullanıcı adına sahip kullanıcı sisteme giriş yaptı.');

                    localStorage.removeItem('locked-user');

                    Router.go('farming.dashboard');
                    that.done();
                }
            });

            return false;
        }
    }
});


Template.lockedPage.helpers({
    lockedSchema: function() {
        return new Schema({
            password: {
                type: String,
                min: 4
            }
        });
    }
});

Template.lockedPage.onRendered(function() {
    $('body').addClass('page-login');
});

Template.lockedPage.onDestroyed(function() {
    $('body').removeClass('page-login');
});