AutoForm.hooks({
    loginForm: {
        onSubmit: function(fields) {
            var that = this;

            Meteor.loginWithPassword(fields.username, fields.password, function(error) {
                if (error) {
                    Notify.error('Kullanıcı bilgileri hatalı!');
                    that.done();
                } else {
                    Logs.info(fields.username + ' kullanıcı adına sahip kullanıcı sisteme giriş yaptı.');

                    Router.go('farming.dashboard');
                    that.done();
                }
            });

            return false;
        }
    }
});


Template.loginPage.helpers({
    loginSchema: function() {
        return new Schema({
            username: {
                type: String,
                regEx: /^[a-z0-9A-Z_]{3,25}$/
            },
            password: {
                type: String,
                min: 4
            }
        });
    }
});