Template.registerHelper('formattedDate', function(date) {
    return moment(new Date(date)).format('DD-MM-YYYY');
});