Template.tabularCheckboxCell.events({
    'change input[type="checkbox"]': function(e) {
        e.stopPropagation();

        var $target = $(e.target),
            $row = $target.closest('tr'),
            name = $target.closest('table').data('name'),
            rows = Session.get(name + '.selectedRows') || [],
            that = this;

        if ($target.is(':checked')) {
            $row.addClass('selected');
            rows.push(this);
        } else {
            _.each(rows, function(element, index) {
                if (element._id == that._id) {
                    rows.splice(index, 1);
                }
            });

            $row.removeClass('selected');
        }

        var $page = $target.closest('.page');
        if ($page.length > 0) {
            var $updateBtn = $page.find('.btn-update:first'),
                $deleteBtn = $page.find('.btn-delete:first');

            if (rows.length > 0) {
                if ($updateBtn.length > 0) {
                    $updateBtn.removeClass('disabled');
                }
                if ($deleteBtn.length > 0) {
                    $deleteBtn.removeClass('disabled');
                }
            } else {
                if ($updateBtn.length > 0) {
                    $updateBtn.addClass('disabled');
                }
                if ($deleteBtn.length > 0) {
                    $deleteBtn.addClass('disabled');
                }
            }
        }

        Session.set(name + '.selectedRows', rows);
    }
});