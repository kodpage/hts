Alert = {};

Alert.custom = function(options) {
    swal(options);
};

Alert.success = function(title, text) {
    swal(title, text, 'success');
};

Alert.error = function(title, text) {
    swal(title, text, 'error');
};

Alert.warning = function(title, text) {
    swal(title, text, 'warning');
};

Alert.confirm = function(title, text, callback) {
    var options = {
        title: title,
        text: text,
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Evet',
        cancelButtonText: 'Hayır'
    };

    swal(options, callback);
};