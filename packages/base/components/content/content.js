var privateVar;

String.prototype.capitalize = function () {
    return this.replace(/(?:^|\s)\S/g, function (a) {
        return a.toUpperCase();
    });
};

var findTitle = function (path) {
    var title = null;

    path = path || Router.current().route.getName();

    // replace all parameters in the title
    title = Router.routes[path].options.title;
    if ('function' === typeof title)
        title = Router.routes[path].options.title();
    var params = Router.current().params;
    if (title) {
        for (var i in params) {
            title = title.replace(
                new RegExp((':' + i).replace(/\+/g, "\\+"), "g"), params[i]);
        }
        if (!Router.routes[path].options.noCaps)
            title = title.capitalize();
    }

    return title;
};

var enrichRouteObject = function (path, isCurrent) {
    // replace all parameters in the title
    var title = findTitle(path) || 'Başlık Yok',
        params = Router.current().params;

    return {
        'path': path,
        'params': params,
        'title': title,
        'isActive': isCurrent,
        'url': Router.routes[path].path(Router.current().params),
        'route': Router.routes[path]
    }
};

var getAllParents = function () {
    if (Router.current().route) {
        var current = Router.current().route.getName();
        var parent = Router.current().route.options.parent;

        if (parent) {
            return getParentParent([enrichRouteObject(current, true), enrichRouteObject(parent)]);
        } else {
            return [enrichRouteObject(current, true)];
        }
    } else { // no routes have been specified
        return [];
    }
};

// parents must be always an array
var getParentParent = function (parents) {
    var newParent = Router.routes[parents[parents.length - 1].path].options.parent;

    if (newParent) {
        parents.push(enrichRouteObject(newParent));

        return getParentParent(parents);
    } else {
        return parents;
    }
};

Template.content.helpers({
    isShown: function () {
        return this.hasToolbar !== 'no';
    },

    breadcrumbs: function () {
        return getAllParents().reverse();
    },

    current: function () {
        return _.last(getAllParents().reverse());
    },

    pageTitle: function () {
        return findTitle();
    },

    hasAction: function (action) {
        var actions = this.actions && this.actions.split(',').map(function (a) {
                return a.trim();
            });
        var permission = (action) ? Router.current().route.getName() + '.' + action : Router.current().route.getName();

        // Check permissions
        if (Meteor.user() && !_.contains(['save', 'cancel'], action) && !Meteor.user().hasPermission(permission)) {
            return false;
        }

        if (action) {
            return _.contains(actions, action);
        } else {
            return _.isArray(actions) && actions.length > 1;
        }
    }
});

Template.content.events({
    'click .page-header-actions .btn-save': function(e, t) {
        $(e.target).closest('.page').find('form:first').trigger('submit');
    },

    'click .page-header-actions .btn-cancel': function() {
        history.back();
    }
});

Template.content.onRendered(function() {
    this.$('[data-toggle="tooltip"]').tooltip();
});