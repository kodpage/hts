Menu = {};

var items = new ReactiveObject({});

_.extend(Menu, {

    set: function(path, data) {
        var keyPath = path ? Helpers.convertDotsToArray(path) : [];

        check(data, Match.ObjectIncluding({
            title: String,
            name: String
        }));

        items.set(keyPath, data);
    },

    get: function(path) {
        var menuItems = [];

        path = path && Helpers.convertDotsToArray(path);

        _.each(items.get(path), function(value, key) {
            if (_.isObject(value)) {
                var name = Router.current().route.getName(),
                    hasChild = false,
                    isActive = name == value.name;

                var parent = Router.current().route.options.parent;
                if (!isActive && parent && parent == value.name) {
                    isActive = true;
                }

                _.each(value, function(v, k) {
                    if (_.isObject(v)) {
                        hasChild = true;

                        if (name == v.name) {
                            isActive = true;
                        } else {
                            if (parent && parent == v.name) {
                                isActive = true;
                            }
                        }
                    }
                });

                value.code = key;
                value.hasChild = hasChild;
                value.isActive = isActive;

                // Check user permission
                if (value.name == 'farming.dashboard') {
                    menuItems.push(value);
                } else {
                    if (Meteor.user() && Meteor.user().hasPermission(value.name)) {
                        menuItems.push(value);
                    }else if (value.hasChild) {
                        menuItems.push(value);
                    } else if (value.name.indexOf('.') === -1) {
                        menuItems.push(value);
                    }
                }
            }
        });

        return _.sortBy(menuItems, 'order');
    },

    sections: function() {
        return this.get();
    }

});