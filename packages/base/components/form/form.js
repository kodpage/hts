AutoForm.addHooks(null, {
    onError: function(formType, error) {
        var message = error.error || error.message;

        if (_.isNumber(message)) {
            message = error.message;
        }

        Notify.error(message);
    }
});