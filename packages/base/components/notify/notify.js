Notify = {};

toastr.options = {
    closeButton: true,
    newestOnTop: false,
    hideDuration: 600,
    positionClass: 'toast-top-full-width',
    iconClasses: {
        error: 'toast-danger',
        info: 'toast-info',
        success: 'toast-success',
        warning: 'toast-warning'
    },
    preventDuplicates: true
};

Notify.success = function(text, title, position) {
    title = title || 'Başarılı';

    toastr.success(text, title);
};

Notify.error = function(text, title, position) {
    title = title || 'Hata';

    toastr.error(text, title);
};

Notify.info = function(text, title, position) {
    title = title || 'Bigi';

    toastr.info(text, title);
};

Notify.warning = function(text, title, position) {
    title = title || 'Uyarı';

    toastr.warning(text, title);
};

Notify.system = function(text, title, position) {
    title = title || 'Sistem';

    toastr.system(text, title);
};

Notify.dark = function(text, title, position) {
    toastr.dark(text, title);
};