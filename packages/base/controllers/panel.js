PanelController = BaseController.extend({
    layoutTemplate: 'panelLayout',
    loadingTemplate: 'loading',

    yieldRegions: {
        'panelNavbar': {to: 'panel-navbar'},
        'panelMenubar': {to: 'panel-menubar'},
        'panelToolbar': {to: 'panel-toolbar'},
        'panelFooter': {to: 'panel-footer'}
    },

    waitOn: function() {
        return [
            Subs.subscribe('system.settings'),
            Subs.subscribe('system.latestNotifications'),
            Subs.subscribe('system.notificationCount')
        ];
    },

    onBeforeAction: function() {
        // Check user
        if (!Meteor.userId() && !localStorage.getItem('locked-user')) {
            // if the user is not logged in, got to the Login page
            Router.go('login');
        }

        this.next();
    }
});