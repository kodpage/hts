BaseController = RouteController.extend({

    onAfterAction: function() {

        //----------------------
        //  Set page title
        //----------------------
        if (_.has(this.route.options, 'title')) {
            if (!_.isUndefined(this.route.options.title)) {
                if (_.isString(this.route.options.title)) {
                    document.title = this.route.options.title;
                } else if (_.isFunction(this.route.options.title)) {
                    document.title = this.route.options.title.apply(this);
                }
            }
        } else if (_.has(Router.options, 'title')) {
            if (!_.isUndefined(Router.options.title)) {
                if (_.isString(Router.options.title)) {
                    document.title = Router.options.title;
                } else if (_.isFunction(Router.options.title)) {
                    document.title = Router.options.title.apply(this);
                }
            }
        }

        //----------------------
        //  Set page class
        //----------------------
        var $body = $('body'),
            currentRouteName = this.route.options.name,
            currentClasses = ($body.attr('class')) ? $body.attr('class').split(' ') : [];

        // Replace dots
        currentRouteName = currentRouteName.replace('.', '-');

        // Remove the existing page classes
        currentClasses.map(function(c, i) {
            if (c.indexOf('page-') !== -1) {
                currentClasses.splice(i, 1);
            }
        });

        // Add the page class to the current classes
        currentClasses.push('page-' + currentRouteName);

        // Reset the body classes
        $body.attr('class', currentClasses.join(' '));
    }

});