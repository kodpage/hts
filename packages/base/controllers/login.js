LoginController = BaseController.extend({
    layoutTemplate: 'loginLayout',
    template: 'loginPage',

    onBeforeAction: function() {
        // Check user
        if (Meteor.userId()) {
            // if the user is logged in, got to the dashboard page
            Router.go('farming.dashboard');
        }

        localStorage.removeItem('locked-user');

        this.next();
    },

    waitOn: function() {
        return [
            AssetLoader.load('/assets/css/pages/login.css')
        ];
    },

    action: function() {
        this.render();
    }
});