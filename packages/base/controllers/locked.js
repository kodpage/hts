LockedController = BaseController.extend({
    layoutTemplate: 'loginLayout',
    template: 'lockedPage',

    onBeforeAction: function() {
        // Check user
        if (Meteor.userId()) {
            // if the user is logged in, got to the dashboard page
            Router.go('farming.dashboard');
        }

        if (!localStorage.getItem('locked-user')) {
            Router.go('login');
        }

        this.next();
    },

    waitOn: function() {
        return [
            AssetLoader.load('/assets/css/pages/login.css')
        ];
    },

    action: function() {
        this.render();
    }
});