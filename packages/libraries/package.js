Package.describe({
    name: 'hts:libraries',
    version: '0.0.1'
});

Npm.depends({
    'xlsx' : '0.8.0',
    'xlsjs' : '0.7.5',
    'chance': '0.8.0'
});


Package.onUse(function(api) {
    api.versionsFrom('1.2.1');


    // ----------------------------------------------
    // DEPENDENCIES
    // ----------------------------------------------
    api.use([
        'underscore',
        'ejson',
        'check',
        'momentjs:moment',
        'aldeed:moment-timezone',
        'iron:router',
        'aldeed:collection2',
        'matb33:collection-hooks',
        'dburles:collection-helpers',
        'meteorhacks:subs-manager',
        'meteorhacks:inject-initial'
    ]);
    api.use([
        'templating',
        'tracker',
        'reactive-var'
    ], 'client');
    api.use([
    ], 'server');
    // ----------------------------------------------


    // ----------------------------------------------
    // PACKAGE
    // ----------------------------------------------
    // Package
    api.addFiles('both.js');

    // Foundation
    api.addFiles('foundation/helpers.js');
    api.addFiles('foundation/subs.js', 'client');
    api.addFiles('foundation/reactive-object.js', 'client');
    api.addFiles('foundation/asset-loader.js', 'client');
    api.addFiles('foundation/chance.js', 'server');
    api.addFiles('foundation/logs.js');

    // Excel
    api.addFiles('excel/utils.js', 'server');
    api.addFiles('excel/workbook.js', 'server');
    api.addFiles('excel/worksheet.js', 'server');
    api.addFiles('excel/excel.js', 'server');

    // Permissions
    api.addFiles('permission/permission.js');
    api.addFiles('permission/permission-client.js', 'client');
    api.addFiles('permission/permission-server.js', 'server');

    // Database
    api.addFiles('database/schema.js');
    api.addFiles('database/model.js');

    // Panel
    api.addFiles('panel/panel.js');
    api.addFiles('panel/panel-server.js', 'server');
    api.addFiles('panel/panel-client.js', 'client');
    // ----------------------------------------------


    // ----------------------------------------------
    // EXPORTS
    // ----------------------------------------------
    // Foundation
    api.export('Helpers');
    api.export('Subs', 'client');
    api.export('ReactiveObject', 'client');
    api.export('AssetLoader', 'client');
    api.export('Chance', 'server');
    api.export('Logs');

    // Excel
    api.export('Excel', 'server');

    // Database
    api.export('Schema');
    api.export('Schemas');
    api.export('Model');

    // Permission
    api.export('Permission');
    // ----------------------------------------------
});
