Model = {};

/**
 * Creates new model
 *
 * @param name
 * @param options
 * @returns {Mongo.Collection}
 */
Model.extend = function(name, options) {
    options = options || {};

    var schema = options.schema || null;
    var helpers = options.helpers || {};
    delete options.helpers;
    var collection = new Mongo.Collection(name, options);

    // Apply schema
    if (_.isObject(schema)) {
        check(schema, Schema);

        collection.attachSchema(schema);
    }

    // Add Allow Deny Rules - utilize permissions
    if (Meteor.isServer && _.isString(options.permission)) {
        collection.attachPermissions(options.permission);
    }

    // Add helpers
    collection.helpers(helpers);

    return collection;
};