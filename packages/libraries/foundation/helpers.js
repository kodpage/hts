Helpers = {};

/**
 * Searches a object with a given string
 *
 * @param object
 * @param key
 * @param selectFirstIfIsArray
 * @returns {*}
 */
Helpers.searchObjectWithDots = function(object, key, selectFirstIfIsArray) {
    key = key.split('.');

    try {
        for (var i = 0; i < key.length; i++) {
            if (selectFirstIfIsArray && object.length && object.length > 0) {
                object = object[0];
            }
            if (key[i] in object) {
                object = object[key[i]];
            } else {
                return null;
            }
        }
    } catch (error) {
        return null;
    }

    return object || null;
};

/**
 * Convert given dotted string to an array
 *
 * @param key
 * @returns {Array}
 */
Helpers.convertDotsToArray = function(key) {
    var data = [];

    if (key.indexOf('.') !== -1) {
        key = key.split('.');

        for (var i = 0; i < key.length; i++) {
            data.push(key[i]);
        }
    } else {
        data.push(key);
    }

    return data;
};

/**
 * Deep extend
 *
 * @param target
 * @param source
 * @returns {*}
 */
Helpers.deepExtend = function(target, source) {
    for (var prop in source) {
        if (source.hasOwnProperty(prop)) {
            if (typeof target[prop] == 'object' && typeof source[prop] == 'object') {
                this.deepExtend(target[prop], source[prop]);
            } else {
                target[prop] = source[prop];
            }
        }
    }

    return target;
};