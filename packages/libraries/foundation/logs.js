Logs = {};

Logs._collection = new Mongo.Collection('logs');

Logs._log = function(type, msg) {
    if (Meteor.isClient) {
        Meteor.call('system.logs.write', type, msg);
    } else {
        Logs._collection.insert({
            userId: Meteor.userId(),
            date: moment().toDate(),
            type: type,
            msg: msg
        })
    }
};

if (Meteor.isServer) {
    Meteor.methods({
        'system.logs.write': function(type, msg) {
            Logs._log(type, msg);
        }
    });
}


Logs.info = function(msg) {
    this._log('info', msg);
};

Logs.error = function(msg) {
    this._log('error', msg);
};

Logs.warning = function(msg) {
    this._log('warning', msg);
};