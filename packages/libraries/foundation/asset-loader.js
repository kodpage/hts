/**
 * Kodpanel
 *
 * New generation server control panel.
 *
 * @package kodpanel
 * @author Mahmut AKTAS <kodpage@icloud.com>
 */

AssetLoader = {
    _readyDeps: new Tracker.Dependency,
    _assets: [],
    _loadList: [],

    load: function(assets) {
        var that = this,
            hasNew = false;

        assets = _.isArray(assets) ? assets : [assets];

        _.each(assets, function(asset) {
            var data = {},
                isInList = false;

            _.each(that._assets, function(item) {
                if (asset == item.src) {
                    isInList = true;
                }
            });

            if (!isInList) {
                hasNew = true;

                data.src = asset;
                data.type = /\.css$/i.test(asset) ? 'text' : 'script';
                data.ready = false;

                that._assets.push(data);
                that._loadList.push(data);
            }
        });

        return this._loadAsset(hasNew);
    },

    _loadAsset: function(hasNew, asset) {
        var that = this;

        if (hasNew) {
            asset = asset || this._assets[0];

            $.ajax({
                url: asset.src,
                dataType: 'text',
                cache: true,
                success: function(data) {
                    that._loadList.shift();

                    _.each(that._assets, function(item, index) {
                        if (item.src == asset.src) {
                            that._assets[index].ready = true;
                            that._readyDeps.changed();
                        }
                    });

                    if (asset.type == 'text') {
                        if ($('style[data-src="' + asset.src + '"]').length < 1) {
                            $('head').append('<style data-src="' + asset.src + '">' + data + '</style>');
                        }
                    } else if (asset.type == 'script') {
                        if ($('script[data-src="' + asset.src + '"]').length < 1) {
                            $('head').append('<script type="text/javascript" data-src="' + asset.src + '">' + data + '</script>');
                        }
                    }

                    if (that._loadList.length > 0) {
                        that._loadAsset(true, that._loadList[0]);
                    }
                }
            });
        }

        return {
            ready: function() {
                var ready = true;

                _.each(that._assets, function(asset) {
                    if (!asset.ready) {
                        ready = false;
                    }
                });

                that._readyDeps.depend();

                return ready;
            }
        };
    }
};

Router.onBeforeAction(function () {
    AssetLoader._assets = [];
    AssetLoader._loadList = [];

    this.next();
});