Permission._collection._ensureIndex('name', {unique: 1});

/**
 * Publish all permissions to all clients. No subscription is required.
 */
Meteor.publish(null, function() {
    return Permission._collection.find({});
});

/**
 * Always publish logged-in user's permissions so client-side
 * checks can work.
 */
Meteor.publish(null, function() {
    var userId = this.userId,
        fields = {permissions: 1};

    return Meteor.users.find({_id: userId}, {fields: fields});
});

/**
 * Adds permission
 */
Permission.add = function(data) {

    data.group = data.group || 'Mixed';

    check(data, Match.ObjectIncluding({
        label: String,
        name: String,
        group: String
    }));

    Permission._collection.update({name: data.name}, {$set: data}, {upsert: true});
};

/**
 * Removes permission
 */
Permission.remove = function(name) {
    check(name, String);

    return Permission._collection.remove({name: name});
};

Permission.addUserPermission = function(userId, permissions) {
    check(userId, String);
    check(permissions, Match.OneOf(String, Array));

    if (!_.isArray(permissions)) {
        permissions = [permissions];
    }

    return Meteor.users.update({_id: userId}, {$addToSet: {permissions: {$each: permissions}}});
};