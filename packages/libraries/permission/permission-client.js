/**
 * Has permission from the client
 */
Template.registerHelper('hasPermission', function(permission) {
    return Meteor.user().hasPermission(permission);
});