/**
 * Initialize variables
 */
Permission = {};
Permission._permissions = {};

/**
 * To save the permissions in the database
 */
Permission._collection =  new Mongo.Collection('permissions');
Permission._collection.attachSchema(new SimpleSchema({
    name: {
        type: String
    },
    label: {
        type: String
    },
    group: {
        type: String
    }
}));

/**
 * Adds helpers to users
 */
Meteor.users.helpers({
    /**
     * Returns the user permissions
     */
    permissions: function() {
        var that = this;

        return Permission._collection.find({name: {$in: that.permissions}}).fetch();
    },

    /**
     * To check if the user has a permission
     */
    hasPermission: function(permission) {
        return _.contains(this.permissions, 'admin') || _.contains(this.permissions, permission);
    }
});


Mongo.Collection.prototype.attachPermissions = function(name) {
    this.allow({
        insert: function() {
            return Meteor.user().hasPermission(name + '.create');
        },
        update: function() {
            return Meteor.user().hasPermission(name + '.update');
        },
        remove: function() {
            return Meteor.user().hasPermission(name + '.delete');
        }
    });

    this.deny({
        insert: function() {
            return !Meteor.user().hasPermission(name + '.create');
        },
        update: function() {
            return !Meteor.user().hasPermission(name + '.update');
        },
        remove: function() {
            return !Meteor.user().hasPermission(name + '.delete');
        }
    });
};